using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorTrigger : MonoBehaviour
{

    public GameObject doorOpen;
    public GameObject doorClosed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            GetComponent<AudioSource>().Play();
            doorOpen.GetComponent<SpriteRenderer>().enabled = true;
            doorClosed.SetActive(false);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        GetComponent<AudioSource>().Play();
        doorOpen.GetComponent<SpriteRenderer>().enabled = false;
        doorClosed.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
