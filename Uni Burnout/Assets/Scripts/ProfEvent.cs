using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProfEvent : MonoBehaviour
{
    public GameObject minuteHand;
    public List<GameObject> UI;

    public GameObject assignment001;
    public GameObject assignment002;
    public GameObject assignment003;

    public GameObject prof001;
    public GameObject prof002;
    public GameObject prof003;

    public Sprite dilkmore001;
    public Sprite dilkmore002;
    public Sprite dilkmore003;
    public Sprite dilkmore004;

    public GameObject skipText;

    private bool fallAssignment001 = false;
    private bool fallAssignment002 = false;
    private bool fallAssignment003 = false;

    public AudioClip fallsound;
    public AudioClip openingSound;

    private bool skipIntro = false;

    private int sequenceSkip = 0;

    private Dialogue ds;

    IEnumerator dilkmore;
    IEnumerator wegener;
    IEnumerator galifianakis;

    // Start is called before the first frame update
    void Start()
    {
        GameObject dialogSystem = GameObject.Find("DialogSystem");
        ds = dialogSystem.GetComponent<Dialogue>();
        skipText.SetActive(true);

        int image = Random.Range(0, 4);

        switch (image)
        {
            case 0:
                prof003.GetComponent<SpriteRenderer>().sprite = dilkmore001;
                break;
            case 1:
                prof003.GetComponent<SpriteRenderer>().sprite = dilkmore002;
                break;
            case 2:
                prof003.GetComponent<SpriteRenderer>().sprite = dilkmore003;
                break;
            case 3:
                prof003.GetComponent<SpriteRenderer>().sprite = dilkmore004;
                break;
        }

        StartCoroutine(OpeningSound());
        dilkmore = Dilkmore();

        for (int i = 0; i < UI.Count; i++)
        {
            UI[i].SetActive(false);
        }

        PlayerMovement.gameOver = true;
        minuteHand.GetComponent<Animator>().SetBool("WorkingClock", false);
        StartCoroutine(dilkmore); 
    }

    // Update is called once per frame
    void Update()
    {
        if (fallAssignment001)
        {
            if (assignment001.transform.localPosition.y > -0.421)
            {
                assignment001.transform.position = new Vector3(assignment001.transform.localPosition.x,
                    assignment001.transform.localPosition.y - 0.004f * 3f, assignment001.transform.localPosition.z);
                    //assignment001.transform.localPosition.y - 0.004f, assignment001.transform.localPosition.z);
            }
        }

        if (fallAssignment002)
        {
            if (assignment002.transform.localPosition.y > -0.421)
            {
                assignment002.transform.position = new Vector3(assignment002.transform.localPosition.x,
                    assignment002.transform.localPosition.y - 0.004f * 3f, assignment002.transform.localPosition.z);
                    //assignment002.transform.localPosition.y - 0.004f, assignment002.transform.localPosition.z);
            }
        }

        if (fallAssignment003)
        {
            if (assignment003.transform.localPosition.y > -0.421)
            {
                assignment003.transform.position = new Vector3(assignment003.transform.localPosition.x,
                    assignment003.transform.localPosition.y - 0.004f * 3f, assignment003.transform.localPosition.z);
                    //assignment003.transform.localPosition.y - 0.004f, assignment003.transform.localPosition.z);
            }
        }
        
        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            if (sequenceSkip >= 0 && sequenceSkip <= 0 && !skipIntro)
            {
                sequenceSkip = 1;
                GetComponent<AudioSource>().Stop();
                StopCoroutine(dilkmore);
                ds.StopTyping();
                prof003.SetActive(false);
                fallAssignment001 = true;
                assignment001.transform.position = new Vector3(assignment001.transform.localPosition.x, -0.421f, assignment001.transform.localPosition.z);
                prof001.SetActive(true);
                wegener = Wegener();
                StartCoroutine(wegener);
            }
            else if (sequenceSkip == 1 && !skipIntro)
            {
                sequenceSkip = 2;
                GetComponent<AudioSource>().Stop();
                StopCoroutine(wegener);
                ds.StopTyping();
                prof001.SetActive(false);
                fallAssignment002 = true;
                assignment002.transform.position = new Vector3(assignment002.transform.localPosition.x, -0.421f, assignment002.transform.localPosition.z);
                prof002.SetActive(true);
                galifianakis = Galifianakis();
                StartCoroutine(galifianakis);
            }
            else if (sequenceSkip == 2 && !skipIntro)
            {
                sequenceSkip = -1;
                GetComponent<AudioSource>().Stop();
                prof002.SetActive(false);
                StopCoroutine(galifianakis);
                ds.StopTyping(); 
                fallAssignment003 = true;
                assignment003.transform.position = new Vector3(assignment003.transform.localPosition.x, -0.421f, assignment003.transform.localPosition.z);
                for (int i = 0; i < UI.Count; i++)
                {
                    UI[i].SetActive(true);
                }

                PlayerMovement.gameOver = false;
                minuteHand.GetComponent<Animator>().SetBool("WorkingClock", true);
                skipIntro = true;
                skipText.SetActive(false);
                Object.Destroy(this.gameObject);
            }
        }
        
        if (Input.GetKeyUp(KeyCode.Space))
        {
            if (!skipIntro)
            {
                StopCoroutine(dilkmore);
                StopAllCoroutines();
                fallAssignment001 = true;
                fallAssignment002 = true;
                fallAssignment003 = true;
                ds.StopTyping();
                prof001.SetActive(false);
                prof002.SetActive(false);
                prof003.SetActive(false);
                for (int i = 0; i < UI.Count; i++)
                {
                    UI[i].SetActive(true);
                }

                PlayerMovement.gameOver = false;
                minuteHand.GetComponent<Animator>().SetBool("WorkingClock", true);
                assignment001.transform.position = new Vector3(assignment001.transform.localPosition.x,
                       -0.421f, assignment001.transform.localPosition.z);
                assignment002.transform.position = new Vector3(assignment002.transform.localPosition.x,
                       -0.421f, assignment002.transform.localPosition.z);
                assignment003.transform.position = new Vector3(assignment003.transform.localPosition.x,
                       -0.421f, assignment003.transform.localPosition.z);
                GetComponent<AudioSource>().Stop();
                skipIntro = true;
                skipText.SetActive(false);
                Object.Destroy(this.gameObject);
            }
        }
    }

    IEnumerator Dilkmore()
    {
        yield return new WaitForSeconds(2);
        prof003.SetActive(true);
        yield return new WaitForSeconds(1);
        ds.Display("Prof. Dilkmore: \"Finish the assignments of 'Mobile Games' until the 3rd day!\"", 0.02f, 4f);
        yield return new WaitForSeconds(6f);
        fallAssignment001 = true;
        GetComponent<AudioSource>().clip = fallsound;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(3);
        prof003.SetActive(false);
        wegener = Wegener();
        StartCoroutine(wegener);
    }

    IEnumerator Wegener()
    {
        yield return new WaitForSeconds(2);
        prof001.SetActive(true);
        yield return new WaitForSeconds(1);
        ds.Display("Markus Wegener: \"Finish the assignments of 'Deep Learning' until the 3rd day!\"", 0.02f, 4f);
        yield return new WaitForSeconds(6f);
        fallAssignment002 = true;
        GetComponent<AudioSource>().clip = fallsound;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(3);
        prof001.SetActive(false);
        galifianakis = Galifianakis();
        StartCoroutine(Galifianakis());
    }

    IEnumerator Galifianakis()
    {
        yield return new WaitForSeconds(2);
        prof002.SetActive(true);
        yield return new WaitForSeconds(1);
        ds.Display("Prof. Galifianakis: \"Finish the assignments of 'Data Analysis' until the 4th day!\"", 0.02f, 4f);
        yield return new WaitForSeconds(6f);
        fallAssignment003 = true;
        GetComponent<AudioSource>().clip = fallsound;
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(3);
        prof002.SetActive(false);
        yield return new WaitForSeconds(1);
        for (int i = 0; i < UI.Count; i++)
        {
            UI[i].SetActive(true);
        }

        PlayerMovement.gameOver = false;
        minuteHand.GetComponent<Animator>().SetBool("WorkingClock", true);
        skipIntro = true;
        skipText.SetActive(false);
        Object.Destroy(this.gameObject);
    }

    IEnumerator OpeningSound()
    {
        yield return new WaitForSeconds(0.5f);
        GetComponent<AudioSource>().clip = openingSound;
        GetComponent<AudioSource>().Play();
    }

}
