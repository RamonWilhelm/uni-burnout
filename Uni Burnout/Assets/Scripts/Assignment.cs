using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using System;

public class Assignment : MonoBehaviour
{
    public static int assignments = 0;
    public static int compl_assignments = 0;
    private int crossindex = 0;
    private bool risingStresslevel = false;
    public GameObject stresslevel;
    private bool work = false;
    private GameObject player;
    private Animator anim;
    public GameObject assignment;
    private int numPaper = 0;
    private int numPaperMax = 50;
    private float stressMax = 0.000495f / 2;
    private float stressMax2;
    private float stressFactor;
    public GameObject healthbar;
    public List<Sprite> paperSprites;
    public TextMeshProUGUI actionText;
    public string text;
    public bool isFinished = false;
    public int deadlineDay;
    private bool workable = true;
    public GameObject redCross;
    public GameObject greenCross;
    public AudioClip gameOverSound;
    public GameObject burnout;
    public GameObject scoreSystem;
    public List<GameObject> uiObjects;
    public AudioClip bling;
    private bool restart = false;
    public static int numBurnouts = 0;

    public int grade = 5;

    public static DateTime startDateTime;
    public static DateTime endDateTime;

    private bool getTime = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            if (workable)
            {
                work = true;
                actionText.text = text;
            }
            else
            {
                work = false;
                actionText.text = "";
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        work = false;
        actionText.text = "";
    }

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
        anim = player.GetComponent<Animator>();
        stressFactor = 2;
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerMovement.gameOver)
        {
            if (!getTime)
            {
                getTime = true;
                startDateTime = DateTime.Now;
            }
        }
        else
        {
            if (getTime)
            {
                endDateTime = DateTime.Now;
            }
        }

        if (restart)
        {
            if (Input.GetKeyUp(KeyCode.Space))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                resetStatics();
            }

            if (Input.GetKeyUp(KeyCode.Return))
            {
                SceneManager.LoadScene(0);
                resetStatics();
            }
        }

        if (assignments == 3)
        {
            PlayerMovement.gameOver = true;
            PlayerMovement.doing = false;
            anim.SetBool("Walk", false);
            StartCoroutine(ScoreSequence());
            restart = true;
        }

        if (ClockTime.days == deadlineDay && !isFinished)
        {
            PlayerMovement.doing = false;
            risingStresslevel = false;
            anim.SetBool("Work", false);
            isFinished = true;
            workable = false;
            work = false;
            actionText.text = "";
            redCross.SetActive(true);
            assignments += 1;
        }

        if (healthbar.transform.localScale.x <= 0f)
        {
            stressFactor = 4;
        }
        else
        {
            stressFactor = 1;
        }


        if ((ClockTime.counter == 0 && ClockTime.hour >= 8) || (ClockTime.counter == 1 && ClockTime.hour < 8))
        {
            stressMax2 = stressMax * stressFactor / 4;
        }
        else
        {
            stressMax2 = stressMax * stressFactor / 1.2f;
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (work)
            {
                PlayerMovement.doing = true;
                risingStresslevel = true;
                anim.SetBool("Work", true);
            }
        }

        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            if (work)
            {
                PlayerMovement.doing = false;
                risingStresslevel = false;
                anim.SetBool("Work", false);
            }
        }
    

        if (risingStresslevel && stresslevel.transform.localScale.x < 0.21f)
        {
            stresslevel.transform.localScale += new Vector3(stressMax2 * 2f, 0f, 0f);
            //stresslevel.transform.localScale += new Vector3(stressMax2, 0f, 0f);
        }

        if (risingStresslevel && stresslevel.transform.localScale.x > 0.21f)
        {
            PlayerMovement.gameOver = true;
            PlayerMovement.doing = false;
            risingStresslevel = false;
            anim.SetBool("Work", false);
            anim.SetBool("Burnout", true);
            actionText.text = "";
            StartCoroutine(GameOverSequence());
        }

        if (risingStresslevel)
        {
            numPaper += 2;
            //numPaper += 1;
        }

        if (numPaper > numPaperMax && numPaper <= numPaperMax * 40)
        {
            if (crossindex < 1)
            {
                grade = 5;
                crossindex += 1;
                GetComponent<AudioSource>().Play();
                greenCross.GetComponent<Animator>().SetTrigger("Start");
            }
            GetComponent<SpriteRenderer>().sprite = paperSprites[1];
        }
        else if (numPaper > numPaperMax * 4 && numPaper <= numPaperMax * 60)
        {
            if (crossindex < 2)
            {
                grade = 5;
                crossindex += 1;
                GetComponent<AudioSource>().Play();
                greenCross.GetComponent<Animator>().SetTrigger("Start");
            }
            GetComponent<SpriteRenderer>().sprite = paperSprites[2];
        }
        else if (numPaper > numPaperMax * 6 && numPaper <= numPaperMax * 80)
        {
            if (crossindex < 3)
            {
                grade = 5;
                crossindex += 1;
                GetComponent<AudioSource>().Play();
                greenCross.GetComponent<Animator>().SetTrigger("Start");
            }
            GetComponent<SpriteRenderer>().sprite = paperSprites[3];
        }
        else if (numPaper > numPaperMax * 8 && numPaper <= numPaperMax * 100)
        {
            if (crossindex < 4)
            {
                grade = 5;
                crossindex += 1;
                GetComponent<AudioSource>().Play();
                greenCross.GetComponent<Animator>().SetTrigger("Start");
            }
            GetComponent<SpriteRenderer>().sprite = paperSprites[4];
        }
        else if (numPaper > numPaperMax * 10 && numPaper <= numPaperMax * 110)
        {
            if (crossindex < 5)
            {
                grade = 5;
                crossindex += 1;
                GetComponent<AudioSource>().Play();
                greenCross.GetComponent<Animator>().SetTrigger("Start");
            }
            GetComponent<SpriteRenderer>().sprite = paperSprites[5];
        }
        else if (numPaper > numPaperMax * 11 && numPaper <= numPaperMax * 120)
        {
            if (crossindex < 6)
            {
                grade = 5;
                crossindex += 1;
                GetComponent<AudioSource>().Play();
                greenCross.GetComponent<Animator>().SetTrigger("Start");
            }
            GetComponent<SpriteRenderer>().sprite = paperSprites[6];
        }
        else if (numPaper > numPaperMax * 12 && numPaper <= numPaperMax * 130)
        {
            if (crossindex < 7)
            {
                grade = 5;
                crossindex += 1;
                GetComponent<AudioSource>().Play();
                greenCross.GetComponent<Animator>().SetTrigger("Start");
            }
            GetComponent<SpriteRenderer>().sprite = paperSprites[7];
        }
        else if (numPaper > numPaperMax * 13 && numPaper <= numPaperMax * 140)
        {
            if (crossindex < 8)
            {
                grade = 4;
                crossindex += 1;
                GetComponent<AudioSource>().Play();
                greenCross.GetComponent<Animator>().SetTrigger("Start");
            }
            GetComponent<SpriteRenderer>().sprite = paperSprites[8];
        }
        else if (numPaper > numPaperMax * 14 && numPaper <= numPaperMax * 150)
        {
            if (crossindex < 9)
            {
                grade = 4;
                crossindex += 1;
                GetComponent<AudioSource>().Play();
                greenCross.GetComponent<Animator>().SetTrigger("Start");
            }
            GetComponent<SpriteRenderer>().sprite = paperSprites[9];
        }
        else if (numPaper > numPaperMax * 15 && numPaper <= numPaperMax * 160)
        {
            if (crossindex < 10)
            {
                grade = 3;
                crossindex += 1;
                GetComponent<AudioSource>().Play();
                greenCross.GetComponent<Animator>().SetTrigger("Start");
            }
            GetComponent<SpriteRenderer>().sprite = paperSprites[10];
        }
        else if (numPaper > numPaperMax * 16 && numPaper <= numPaperMax * 170)
        {
            if (crossindex < 11)
            {
                grade = 3;
                crossindex += 1;
                GetComponent<AudioSource>().Play();
                greenCross.GetComponent<Animator>().SetTrigger("Start");
            }
            GetComponent<SpriteRenderer>().sprite = paperSprites[11];
        }
        else if (numPaper > numPaperMax * 17 && numPaper <= numPaperMax * 180)
        {
            if (crossindex < 12)
            {
                grade = 2;
                crossindex += 1;
                GetComponent<AudioSource>().Play();
                greenCross.GetComponent<Animator>().SetTrigger("Start");
            }
            GetComponent<SpriteRenderer>().sprite = paperSprites[12];
        }
        else if (numPaper > numPaperMax * 18 && numPaper <= numPaperMax * 190)
        {
            if (crossindex < 13)
            {
                grade = 2;
                crossindex += 1;
                GetComponent<AudioSource>().Play();
                greenCross.GetComponent<Animator>().SetTrigger("Start");
            }
            GetComponent<SpriteRenderer>().sprite = paperSprites[13];
        }
        else if (numPaper > numPaperMax * 190)
        {
            if (crossindex < 14)
            {
                grade = 1;
                crossindex += 1;
                GetComponent<AudioSource>().Play();
                greenCross.GetComponent<Animator>().SetTrigger("Start");
            }
            GetComponent<SpriteRenderer>().sprite = paperSprites[14];
            GetComponent<BoxCollider2D>().enabled = false;
            
            if (!isFinished)
            {
                PlayerMovement.doing = false;
                risingStresslevel = false;
                anim.SetBool("Work", false);
                isFinished = true;
                assignments += 1;
                compl_assignments += 1;
            }
        }
    }

    IEnumerator GameOverSequence()
    {
        yield return new WaitForSeconds(0.7f);
        GetComponent<AudioSource>().clip = gameOverSound;
        GetComponent<AudioSource>().Play();
        burnout.SetActive(true);
        numBurnouts += 1;
        restart = true;
    }

    IEnumerator ScoreSequence()
    {
        yield return new WaitForSeconds(2f);
        for (int i = 0; i < uiObjects.Count; i++)
        {
            uiObjects[i].SetActive(false);
        }
        scoreSystem.SetActive(true);
    }

    public void resetStatics()
    {
        compl_assignments = 0;
        assignments = 0;
        ClockTime.hour = 8;
        ClockTime.days = 1;
        ClockTime.counter = 0;
        ClockTime.minute = 0;
        ClockTime.friends = 2;
        ClockTime.reducefriends = false;
        ClockTime.meetingFriends = false;
        ClockTime.friendtriggerActive = false;
        ClockTime.tired = false;
        PlayerMovement.doing = false;
        PlayerMovement.gameOver = false;
        CounterObject.status = "";
        CounterObject.eatCounter = 0;
        CounterObject.sleepCounter = 0;
        CounterObject.meetFriendCounter = 0;
        Refridgerator.foodCount = 3;
    }
}
