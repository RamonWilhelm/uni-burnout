using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FriendScript : MonoBehaviour
{
    public GameObject sign;
    public GameObject stresslevel;
    private bool loweringStresslevel = false;
    private GameObject player;
    private float stressfactor = 0.0000495f / 2;

    private bool meetfriends = false;

    public GameObject friend001;
    public GameObject friend002;
    public GameObject friend003;

    private bool startFriendCounter = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            if (ClockTime.friendtriggerActive)
            {
                if (!startFriendCounter)
                {
                    startFriendCounter = true;
                    CounterObject.meetFriendCounter += 1;
                }

                GetComponent<AudioSource>().Play();
                meetfriends = true;
            }
     
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        
    }

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if (meetfriends)
        {
            friend001.SetActive(false);
            friend002.SetActive(false);
            friend003.SetActive(false);
            PlayerMovement.doing = true;
            ClockTime.meetingFriends = true;
            player.GetComponent<SpriteRenderer>().enabled = false;
            loweringStresslevel = true;
            player.transform.localScale = new Vector3(-5, 5, 1);

            if (Refridgerator.foodCount == 0)
            {
                //sign.SetActive(false);
                Refridgerator.foodCount = 3;
            }

            StartCoroutine(ComeBack());
        }

        if (loweringStresslevel && stresslevel.transform.localScale.x > 0f)
        {
            stresslevel.transform.localScale -= new Vector3(stressfactor * 2 * 4f, 0f, 0f);
            //stresslevel.transform.localScale -= new Vector3(stressfactor * 2, 0f, 0f);
        }
    }

    IEnumerator ComeBack()
    {
        yield return new WaitForSeconds(12f);
        meetfriends = false;
        player.GetComponent<SpriteRenderer>().enabled = true;
        loweringStresslevel = false;
        PlayerMovement.doing = false;
        startFriendCounter = false;
        StopAllCoroutines();
    }
}
