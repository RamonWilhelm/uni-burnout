using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Dialogue : MonoBehaviour
{
    public TextMeshProUGUI displaytext;
    public string sentences;
    public float typingSpeed;
    public float waitText;
    public GameObject background;
    private bool _break;

    IEnumerator co;

    public void Display(string text, float typeSpeed, float waitText)
    {
        StopAllCoroutines();
        StopTyping();

        co = Type();
        this.displaytext.text = "";
        this.sentences = text;
        this.typingSpeed = typeSpeed;
        this.waitText = waitText;
        _break = false;
        StartCoroutine(co);
        
    }

    public void StopTyping()
    {
        co = Type();

        _break = true;
        this.displaytext.text = "";
        this.sentences = "";
        this.typingSpeed = 0;
        this.waitText = 0;
        background.SetActive(false);
        StopCoroutine(co);
    }

    IEnumerator Type()
    {
        background.SetActive(true);
        foreach (char letter in this.sentences.ToCharArray()) 
        {
            if (_break)
            {
                this.sentences = "";
                break;
            }

            displaytext.text += letter;
            yield return new WaitForSeconds(typingSpeed);
        }

        yield return new WaitForSeconds(waitText);
        this.sentences = "";
        this.displaytext.text = "";
        background.SetActive(false);
    }
}
