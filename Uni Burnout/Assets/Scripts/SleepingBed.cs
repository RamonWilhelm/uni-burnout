using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SleepingBed : MonoBehaviour
{
    private bool sleep = false;
    public GameObject stresslevel;
    private bool loweringStresslevel = false;
    private GameObject player;
    public TextMeshProUGUI actiontext;

    public GameObject healthbar;

    public bool startSleepCounter = false;

    private float reduceStress = 0.0000495f / 3.5f;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            sleep = true;
            actiontext.text = "Bed";
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        sleep = false;
        actiontext.text = "";
    }

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        /*
        if ((ClockTime.counter == 0 && ClockTime.hour >= 8) || (ClockTime.counter == 1 && ClockTime.hour < 8))
        {
            if (sleep)
            {
                startSleepCounter = false;
                PlayerMovement.doing = false;
                GetComponent<SpriteRenderer>().enabled = false;
                player.GetComponent<SpriteRenderer>().enabled = true;
                loweringStresslevel = false;
            }
        }*/

        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            if (sleep)
            {
                //if ((ClockTime.counter == 1 && ClockTime.hour >= 8) || (ClockTime.counter == 0 && ClockTime.hour < 8))
                //{
                    startSleepCounter = false;
                    PlayerMovement.doing = false;
                    GetComponent<SpriteRenderer>().enabled = false;
                    player.GetComponent<SpriteRenderer>().enabled = true;
                    loweringStresslevel = false;
                /*}
                else
                {
                    GameObject dialogSystem = GameObject.Find("DialogSystem");
                    Dialogue ds = dialogSystem.GetComponent<Dialogue>();
                    ds.displaytext.text = "";
                    ds.sentences = "";
                    ds.finishSentence = false;
                    ds.StopTyping();
                    ds.Display("I'm not tired right now!", 0.02f, 1f);
                    PlayerMovement.doing = false;
                    GetComponent<SpriteRenderer>().enabled = false;
                    player.GetComponent<SpriteRenderer>().enabled = true;
                    loweringStresslevel = false;
                }*/
            }
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (sleep)
            {
                //if ((ClockTime.counter == 1 && ClockTime.hour >= 8) || (ClockTime.counter == 0 && ClockTime.hour < 8))
                //{
                    PlayerMovement.doing = true;
                    GetComponent<SpriteRenderer>().enabled = true;
                    player.GetComponent<SpriteRenderer>().enabled = false;
                    loweringStresslevel = true;

                    if (!startSleepCounter)
                    {
                        startSleepCounter = true;
                        CounterObject.sleepCounter += 1;
                    }
                //}
            }
        }
        

        if (loweringStresslevel && stresslevel.transform.localScale.x > 0f)
        {
            stresslevel.transform.localScale -= new Vector3(reduceStress * 15f, 0f, 0f);
            //stresslevel.transform.localScale -= new Vector3(reduceStress, 0f, 0f);
        }

        if (healthbar.transform.localScale.x < 1f && !PlayerMovement.gameOver)
        {
            if (loweringStresslevel)
            {
                //healthbar.transform.localScale -= new Vector3(0.0001000f, 0f, 0f);
                healthbar.transform.localScale += new Vector3(0.0001000f * 3f * 4f, 0f, 0f);
            }
        }
    }

}
