using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyingFood : MonoBehaviour
{
    private GameObject player;
    private bool storesClosed = false;
    private bool needFood = false;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            if (Refridgerator.foodCount < 3 && !ClockTime.tired)
            {
                needFood = true;
                Refridgerator.foodCount = 3;
                PlayerMovement.doing = true;
                player.GetComponent<SpriteRenderer>().enabled = false;
                player.transform.localScale = new Vector3(-5, 5, 1);
                StartCoroutine(ComeBack());
            }
            else if (ClockTime.tired)
            {
                if (!storesClosed)
                {
                    storesClosed = true;
                    GameObject dialogSystem = GameObject.Find("DialogSystem");
                    Dialogue ds = dialogSystem.GetComponent<Dialogue>();
                    ds.Display("The stores are closed.", 0.02f, 1.5f);
                }
            }
            else 
            {
                if (!needFood)
                {
                    GameObject dialogSystem = GameObject.Find("DialogSystem");
                    Dialogue ds = dialogSystem.GetComponent<Dialogue>();
                    ds.Display("I've got enough food.", 0.02f, 1.5f);
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        storesClosed = false;
        needFood = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator ComeBack()
    {
        yield return new WaitForSeconds(5);
        PlayerMovement.doing = false;
        player.GetComponent<SpriteRenderer>().enabled = true;
        needFood = false;
    }
}
