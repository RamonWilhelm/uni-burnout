using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BedScript : MonoBehaviour
{

    public Sprite bedOpen;
    public Sprite bedClose;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if ((ClockTime.counter == 1 && ClockTime.hour >= 8) || (ClockTime.counter == 0 && ClockTime.hour < 8))
        {
            GetComponent<SpriteRenderer>().sprite = bedOpen;
        }
        else
        { 
            GetComponent<SpriteRenderer>().sprite = bedClose;
        }
    }
}
