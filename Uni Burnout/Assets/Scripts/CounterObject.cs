using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class CounterObject : MonoBehaviour
{
    public static string status = "";
    public static int eatCounter = 0;
    public static int sleepCounter = 0;
    public static int meetFriendCounter = 0;

    public GameObject assignment001;
    public GameObject assignment002;
    public GameObject assignment003;

    public TextMeshProUGUI mgGrade;
    public TextMeshProUGUI dlGrade;
    public TextMeshProUGUI daGrade;
    public TextMeshProUGUI statusText;
    public TextMeshProUGUI compAss;
    public TextMeshProUGUI eat;
    public TextMeshProUGUI sleep;
    public TextMeshProUGUI friends;
    public TextMeshProUGUI burnouts;
    public TextMeshProUGUI playedTime;

    private string str_status = "Status                : ";
    private string str_compAss = "Completed Assignments : ";
    private string str_mgGrade = "Mobile Games Grade    : ";
    private string str_dlGrade = "Deep Learning Grade   : ";
    private string str_daGrade = "Data Analysis Grade   : ";
    private string str_eat = "Food Eaten            : ";
    private string str_sleep = "Slept Times           : ";
    private string str_friends = "Time With Friends     : ";
    private string str_burnouts = "Suffered Burnouts     : ";
    private string str_playedTime = "Played Time           : ";

    private string grade_mg = "";
    private string grade_dl = "";
    private string grade_da = "";

    // Start is called before the first frame update
    void Start()
    {
        //assignment001.GetComponent<Assignment>().numPaper
    }

    // Update is called once per frame
    void Update()
    {
        float mg_grade = assignment001.GetComponent<Assignment>().grade;
        float dl_grade = assignment002.GetComponent<Assignment>().grade;
        float da_grade = assignment003.GetComponent<Assignment>().grade;

        float avg_grade = (mg_grade + dl_grade + da_grade) / 3f;

        if (mg_grade == 0)
        {
            grade_mg = "F";
        }
        else if (mg_grade == 4)
        {
            grade_mg = "D";
        }
        else if (mg_grade == 3)
        {
            grade_mg = "C";
        }
        else if (mg_grade == 2)
        {
            grade_mg = "B";
        }
        else if (mg_grade == 1)
        {
            grade_mg = "A";
        }

        if (dl_grade == 0)
        {
            grade_dl = "F";
        }
        else if (dl_grade == 4)
        {
            grade_dl = "D";
        }
        else if (dl_grade == 3)
        {
            grade_dl = "C";
        }
        else if (dl_grade == 2)
        {
            grade_dl = "B";
        }
        else if (dl_grade == 1)
        {
            grade_dl = "A";
        }

        if (da_grade == 0)
        {
            grade_da = "F";
        }
        else if (da_grade == 4)
        {
            grade_da = "D";
        }
        else if (da_grade == 3)
        {
            grade_da = "C";
        }
        else if (da_grade == 2)
        {
            grade_da = "B";
        }
        else if (da_grade == 1)
        {
            grade_da = "A";
        }

        TimeSpan timespan = Assignment.endDateTime - Assignment.startDateTime;
        playedTime.text = str_playedTime + timespan.Hours + ":" + timespan.Minutes + ":" + timespan.Seconds;
        burnouts.text = str_burnouts + Assignment.numBurnouts;
        compAss.text = str_compAss + Assignment.compl_assignments;
        mgGrade.text = str_mgGrade + grade_mg;
        dlGrade.text = str_dlGrade + grade_dl;
        daGrade.text = str_daGrade + grade_da;
        eat.text = str_eat + eatCounter;
        sleep.text = str_sleep + sleepCounter;
        friends.text = str_friends + meetFriendCounter;

        if ((eatCounter == 0 && sleepCounter == 0 && meetFriendCounter == 0 && (int) avg_grade == 0 ))
        {
            statusText.text = str_status + "Underarchiever";
        }
        else if ((eatCounter == 0 && sleepCounter != 0 && meetFriendCounter == 0 && (int) avg_grade == 0))
        {
            statusText.text = str_status + "Lazybones";
        }
        else if ((eatCounter == 0 && sleepCounter == 0 && meetFriendCounter == 0) && (int) avg_grade != 0)
        {
            statusText.text = str_status + "Basement Child";
        }
        else if ((eatCounter / 2f) >= sleepCounter + meetFriendCounter)
        {
            statusText.text = str_status + "Stress Eater";
        }
        else if ((meetFriendCounter * 5f) >= sleepCounter + eatCounter)
        {
            statusText.text = str_status + "Party Animal";
        }
        else if (sleepCounter >= meetFriendCounter + eatCounter)
        {
            statusText.text = str_status + "Workoholic";
        }
    }
}
