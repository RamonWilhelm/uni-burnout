using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Refridgerator : MonoBehaviour
{
    public GameObject stresslevel;
    private bool loweringStresslevel = false;

    public List<Sprite> fridge;
    public GameObject openFridge;

    private bool fridgeCanBeOpened;
    public static int foodCount;
    private bool fridgeIsOpened;

    public TextMeshProUGUI fcounter;

    public TextMeshProUGUI actiontext;

    public GameObject healthbar;

    public GameObject sign;

    public GameObject hungerbar;

    private float reduceStress = 0.0000495f * 2f;

    private bool sayText = false;

    private float energieFact = 0.1f;

    private bool reduceEnergie = false;


    // Start is called before the first frame update
    void Start()
    {
        foodCount = 3;
        fcounter.text = "FOOD: " + foodCount.ToString();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            fridgeCanBeOpened = true;
            actiontext.text = "Refrigerator";
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        fridgeCanBeOpened = false;
        actiontext.text = "";
    }

    // Update is called once per frame
    void Update()
    {

        fcounter.text = "FOOD: " + foodCount.ToString();


        if (foodCount == 3)
        {
            openFridge.GetComponent<SpriteRenderer>().sprite = fridge[0];
            //sign.SetActive(false);
        }

        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            if (fridgeCanBeOpened)
            {
                if (!fridgeIsOpened)
                {
                    GetComponent<AudioSource>().Play();
                    fridgeIsOpened = true;
                    openFridge.SetActive(true);
                    StartCoroutine(CloseFridge());

                    if (foodCount > 0)
                    {
                        CounterObject.eatCounter += 1;
                        foodCount -= 1;
                        loweringStresslevel = true;
                        energieFact = 1f;
                        reduceEnergie = false;
                        healthbar.transform.localScale += new Vector3(0.05f, 0, 0);
                        hungerbar.transform.localScale = new Vector3(1, 1, 1);
                    }
                }
            }
        }

        if (loweringStresslevel && stresslevel.transform.localScale.x > 0f)
        {
            stresslevel.transform.localScale -= new Vector3(reduceStress * 3f, 0f, 0f);
            //stresslevel.transform.localScale -= new Vector3(reduceStress, 0f, 0f);
        }

        if (healthbar.transform.localScale.x > 0f && !PlayerMovement.gameOver)
        {
            sayText = false;
            //healthbar.transform.localScale -= new Vector3(0.0001000f, 0f, 0f);
            healthbar.transform.localScale -= new Vector3(0.0001000f * energieFact, 0f, 0f);
        }

        if (hungerbar.transform.localScale.x > 0f && !PlayerMovement.gameOver)
        {
            //healthbar.transform.localScale -= new Vector3(0.0001000f, 0f, 0f);
            hungerbar.transform.localScale -= new Vector3(0.0001000f * 3f, 0f, 0f);
            reduceEnergie = false;
        }

        if (hungerbar.transform.localScale.x < 0f && !PlayerMovement.gameOver)
        {
            //healthbar.transform.localScale -= new Vector3(0.0001000f, 0f, 0f);
            if (!reduceEnergie)
            {
                reduceEnergie = true;
                energieFact = 6f;
            }
        }

        if (healthbar.transform.localScale.x < 0f && !PlayerMovement.gameOver)
        {
            if (!sayText)
            {
                sayText = true;
                GameObject dialogSystem = GameObject.Find("DialogSystem");
                Dialogue ds = dialogSystem.GetComponent<Dialogue>();
                ds.Display("My body feels weak.", 0.02f, 1f);
            }
        }
    }  

    IEnumerator CloseFridge()
    {
        yield return new WaitForSeconds(1);
        openFridge.SetActive(false);

        if (foodCount == 2)
        {
            openFridge.GetComponent<SpriteRenderer>().sprite = fridge[1];
        }
        else if (foodCount == 1)
        {
            openFridge.GetComponent<SpriteRenderer>().sprite = fridge[2];
        }
        else if (foodCount == 0)
        {
            openFridge.GetComponent<SpriteRenderer>().sprite = fridge[3];
            //sign.SetActive(true);
            GameObject dialogSystem = GameObject.Find("DialogSystem");
            Dialogue ds = dialogSystem.GetComponent<Dialogue>();
            ds.Display("I have to buy some food!", 0.02f, 1f);
        }

        fridgeIsOpened = false;
        loweringStresslevel = false;
    }
}
