using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ClockTime : MonoBehaviour
{
    public SpriteRenderer hourHand;
    public static int hour = 8;
    public List<Sprite> sprites;
    public static int days = 1;
    public static int counter = 0;
    public TextMeshProUGUI daysLabel;
    public TextMeshProUGUI friendcounter;
    public static int minute = 0;
    public static int friends = 2;
    public static bool reducefriends = false;
    public static bool meetingFriends = false;
    public static bool friendtriggerActive = false;

    private bool friendsAvailable = false;

    private bool displayMessage = false;
    public static bool tired = false;

    public GameObject friend001;
    public GameObject friend002;
    public GameObject friend003;

    public Sprite dayBackground;
    public Sprite nightBackground;

    public GameObject background;

    private int countDown = 12;
    private bool waitCountDown = false;
    public TextMeshProUGUI counterUI;

    // Start is called before the first frame update
    void Start()
    {
        daysLabel.text = "DAY: " + days.ToString();
        friendcounter.text = "PAL: " + friends.ToString();
    }

    // Update is called once per frame
    void Update()
    {

        if ((counter == 1 && hour >= 8 && minute > 0) || (counter == 1 && hour >= 8 && minute <= 10))
        {
            if (!tired)
            {
                tired = true;

                GameObject dialogSystem = GameObject.Find("DialogSystem");
                Dialogue ds = dialogSystem.GetComponent<Dialogue>();
                ds.Display("I feel tired now!", 0.02f, 1.5f);
                background.GetComponent<SpriteRenderer>().sprite = nightBackground;
            }
        }
        
        if ((counter == 0 && hour >= 7 && minute > 0) || (counter == 0 && hour >= 7 && minute <= 10))
        {
            tired = false;
            friendsAvailable = false;
            meetingFriends = false;
            background.GetComponent<SpriteRenderer>().sprite = dayBackground;
        }


        if (((hour >= 2 && minute > 0 && counter == 1) || (hour >= 2 && minute <= 2 && counter == 1)) && friends > 0 && !meetingFriends)
        {
            friendtriggerActive = true;
            reducefriends = true;
            friend001.SetActive(true);
            friend002.SetActive(true);
            friend003.SetActive(true);

            if (!displayMessage && !friendsAvailable)
            {
                GetComponent<AudioSource>().Play();
                friendsAvailable = true;
                displayMessage = true;
                GameObject dialogSystem = GameObject.Find("DialogSystem");
                Dialogue ds = dialogSystem.GetComponent<Dialogue>();
                ds.Display("My friends are here! They want to hangout with me.", 0.02f, 2f);
                waitCountDown = true;
                counterUI.text = "WAIT: " + countDown;
            }
        }
        
        if ((hour >= 3 && minute > 0 && counter == 1) || (hour >= 3 && minute <= 1 && counter == 1) && friends > 0 && !meetingFriends)
        {
            if (reducefriends)
            {
                if (friendsAvailable)
                {
                    displayMessage = false;
                }

                reducefriends = false;

                if (!meetingFriends)
                {
                    meetingFriends = true;
                    friends -= 1;
                    friendcounter.text = "PAL: " + friends.ToString();
                }
            }
            friendtriggerActive = false;
            friend001.SetActive(false);
            friend002.SetActive(false);
            friend003.SetActive(false);
        }

    }

    public void changeMinute()
    {
        if (!PlayerMovement.gameOver)
        {
            if (minute < 11)
            {
                minute += 1;
            }
            else
            {
                minute = 0;
            }

            if (waitCountDown)
            {
                if (countDown > 0)
                {
                    countDown -= 1;
                    counterUI.text = "WAIT: " + countDown;
                }
                else
                {
                    waitCountDown = false;
                    countDown = 12;
                    counterUI.text = "";
                }
            }
        }
    }


    public void changeHour()
    {
        if (!PlayerMovement.gameOver)
        {
            hour += 1;

            switch (hour)
            {
                case 12:
                    hourHand.sprite = sprites[0];
                    hour = 0;
                    counter += 1;

                    if (counter == 2)
                    {
                        counter = 0;
                        days += 1;
                        daysLabel.text = "DAY: " + days.ToString();
                    }

                    break;
                default:
                    hourHand.sprite = sprites[hour];
                    break;
            }
        }
        else
        {
            GetComponent<Animator>().SetBool("WorkingClock", false);
        }
    }
}
