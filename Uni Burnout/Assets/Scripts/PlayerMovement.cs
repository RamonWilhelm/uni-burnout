using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 1.0f;
    private Animator anim;
    public static bool doing = false;
    public GameObject stresslevel;
    public static bool gameOver = false;

    public AudioClip workSound;
    public AudioClip walkSound;

    private float reduceStress = 0.0000495f / 3.5f;



    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameOver)
        {
            float horizontalInput = 0;

            if (!doing)
            {
                horizontalInput = Input.GetAxis("Horizontal");
                transform.position += new Vector3(horizontalInput, 0, 0) * Time.deltaTime * speed;
            }

            if (horizontalInput > 0.01f)
            {
                transform.localScale = new Vector3(-5, 5, 1);
            }
            else if (horizontalInput < -0.01f)
            {
                transform.localScale = new Vector3(5, 5, 1);
            }

            anim.SetBool("Walk", horizontalInput != 0);

            if (stresslevel.transform.localScale.x > 0f)
            {
                //stresslevel.transform.localScale -= new Vector3(reduceStress, 0f, 0f);
                stresslevel.transform.localScale -= new Vector3(reduceStress * 3f, 0f, 0f);
            }

        }
    }

    public void PlaySound()
    {
        GetComponent<AudioSource>().clip = workSound;
        GetComponent<AudioSource>().Play();
    }

    public void WalkSound()
    {
        GetComponent<AudioSource>().clip = walkSound;
        GetComponent<AudioSource>().Play();
    }

}
