using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameStart : MonoBehaviour
{
    public GameObject startscreen;
    public GameObject instruction001;
    public GameObject instruction002;
    public GameObject instruction003;

    private int page = 0;

    private bool instruction = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            instruction = false;
            page = 0;
            SceneManager.LoadScene(1);
        }

        if (!instruction)
        {
            if (Input.GetKeyUp(KeyCode.I))
            {
                startscreen.SetActive(false);
                instruction001.SetActive(true);
                instruction = true;
            }
        }
        else
        {
            if (Input.GetKeyUp(KeyCode.I))
            {
                startscreen.SetActive(true);
                instruction001.SetActive(false);
                instruction002.SetActive(false);
                instruction003.SetActive(false);
                instruction = false;
                page = 0;
            }

            if (Input.GetKeyUp(KeyCode.LeftArrow))
            {
                if (page == 2)
                {
                    instruction003.SetActive(false);
                    instruction002.SetActive(true);
                    page = 1;
                }
                else if (page == 1)
                {
                    instruction002.SetActive(false);
                    instruction001.SetActive(true);
                    page = 0;
                }
            }

            if (Input.GetKeyUp(KeyCode.RightArrow))
            {
                if (page == 0)
                {
                    instruction001.SetActive(false);
                    instruction002.SetActive(true);
                    page = 1;
                }
                else if (page == 1)
                {
                    instruction002.SetActive(false);
                    instruction003.SetActive(true);
                    page = 2;
                }
            }
        }
    }
}
