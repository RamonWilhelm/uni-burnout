﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CCloseFridgeU3Ed__20_t1475090F6F547C8F6CC74550993C1E1A8EECB28A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CComeBackU3Ed__14_t8D490D27296D92B089F1735C495908DCC4CE24A5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CComeBackU3Ed__7_t85EB3F03ABCCF46458945DCF0BA20CB0E14BAA15_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDilkmoreU3Ed__26_tD14549EAB7016DB0FF928B2F9AE2F2CFD2ED3EBA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGalifianakisU3Ed__28_t880F19F9E66C55B2EEB9F5E5C2A64562596E98C2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGameOverSequenceU3Ed__38_t96FD66B07CFB03D6D8DDE3FF203D6BD2CB139D04_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COpeningSoundU3Ed__29_tB3097080664383B3EAD4A1C113AD9682FDC6904D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CScoreSequenceU3Ed__39_t58C3F4AE9ADE5E40AC91712BA4E5C611E6CC9131_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTypeU3Ed__9_tC61615E885ED8F22B60E31A1D10446F01FEC8C51_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWegenerU3Ed__27_t3927F04344617AE8A5AD0A5D83C4FE88A17A3A46_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct  Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct  CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct  CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct  DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct  RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct  StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct  BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct  IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct  DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct  DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void Assignment_t2EA4CB9A2262E4E8F506A57FDE78131DDB71B2C2_CustomAttributesCacheGenerator_Assignment_GameOverSequence_m053B41B7BE1640A478893749BF18277856B18E56(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGameOverSequenceU3Ed__38_t96FD66B07CFB03D6D8DDE3FF203D6BD2CB139D04_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGameOverSequenceU3Ed__38_t96FD66B07CFB03D6D8DDE3FF203D6BD2CB139D04_0_0_0_var), NULL);
	}
}
static void Assignment_t2EA4CB9A2262E4E8F506A57FDE78131DDB71B2C2_CustomAttributesCacheGenerator_Assignment_ScoreSequence_m108330CC445851E85677F49CA88015EFF056C414(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CScoreSequenceU3Ed__39_t58C3F4AE9ADE5E40AC91712BA4E5C611E6CC9131_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CScoreSequenceU3Ed__39_t58C3F4AE9ADE5E40AC91712BA4E5C611E6CC9131_0_0_0_var), NULL);
	}
}
static void U3CGameOverSequenceU3Ed__38_t96FD66B07CFB03D6D8DDE3FF203D6BD2CB139D04_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGameOverSequenceU3Ed__38_t96FD66B07CFB03D6D8DDE3FF203D6BD2CB139D04_CustomAttributesCacheGenerator_U3CGameOverSequenceU3Ed__38__ctor_m2D5D76631A634FE147DB4F41DD4A0F5265ED58A8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGameOverSequenceU3Ed__38_t96FD66B07CFB03D6D8DDE3FF203D6BD2CB139D04_CustomAttributesCacheGenerator_U3CGameOverSequenceU3Ed__38_System_IDisposable_Dispose_mEE520F987D74AE398C332F13FD99879065A00013(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGameOverSequenceU3Ed__38_t96FD66B07CFB03D6D8DDE3FF203D6BD2CB139D04_CustomAttributesCacheGenerator_U3CGameOverSequenceU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7D28F2C1D6EF1970DD18A275BAEE3E1EFC1C3129(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGameOverSequenceU3Ed__38_t96FD66B07CFB03D6D8DDE3FF203D6BD2CB139D04_CustomAttributesCacheGenerator_U3CGameOverSequenceU3Ed__38_System_Collections_IEnumerator_Reset_m1FD62067D3185FE927F9BF7639452F1851329C41(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGameOverSequenceU3Ed__38_t96FD66B07CFB03D6D8DDE3FF203D6BD2CB139D04_CustomAttributesCacheGenerator_U3CGameOverSequenceU3Ed__38_System_Collections_IEnumerator_get_Current_m185FBBE6329ACE7391C7E879B66FDCB986CD3F77(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CScoreSequenceU3Ed__39_t58C3F4AE9ADE5E40AC91712BA4E5C611E6CC9131_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CScoreSequenceU3Ed__39_t58C3F4AE9ADE5E40AC91712BA4E5C611E6CC9131_CustomAttributesCacheGenerator_U3CScoreSequenceU3Ed__39__ctor_mAEF6264729299089B2280586619F541344088D82(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CScoreSequenceU3Ed__39_t58C3F4AE9ADE5E40AC91712BA4E5C611E6CC9131_CustomAttributesCacheGenerator_U3CScoreSequenceU3Ed__39_System_IDisposable_Dispose_m3A87C2378875C252AD099024C4A3E0C12C6EF8C8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CScoreSequenceU3Ed__39_t58C3F4AE9ADE5E40AC91712BA4E5C611E6CC9131_CustomAttributesCacheGenerator_U3CScoreSequenceU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0D41F2085C7F0D9EFF41E229CE747AF7C8B045CE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CScoreSequenceU3Ed__39_t58C3F4AE9ADE5E40AC91712BA4E5C611E6CC9131_CustomAttributesCacheGenerator_U3CScoreSequenceU3Ed__39_System_Collections_IEnumerator_Reset_mB02F97C1ED30CDB2D4292D0210380CA4933A3360(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CScoreSequenceU3Ed__39_t58C3F4AE9ADE5E40AC91712BA4E5C611E6CC9131_CustomAttributesCacheGenerator_U3CScoreSequenceU3Ed__39_System_Collections_IEnumerator_get_Current_m5518076540D1507B7F206374F1CA5BE66A9ADA62(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BuyingFood_t973EF70C1C16785D647B36B3C7B24B6073345260_CustomAttributesCacheGenerator_BuyingFood_ComeBack_m194C119F380AE46C737A355B4452FA7FF25490F6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CComeBackU3Ed__7_t85EB3F03ABCCF46458945DCF0BA20CB0E14BAA15_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CComeBackU3Ed__7_t85EB3F03ABCCF46458945DCF0BA20CB0E14BAA15_0_0_0_var), NULL);
	}
}
static void U3CComeBackU3Ed__7_t85EB3F03ABCCF46458945DCF0BA20CB0E14BAA15_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CComeBackU3Ed__7_t85EB3F03ABCCF46458945DCF0BA20CB0E14BAA15_CustomAttributesCacheGenerator_U3CComeBackU3Ed__7__ctor_m28EE7E5BAF8359271F86C22BE628B4B9FF2CF186(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CComeBackU3Ed__7_t85EB3F03ABCCF46458945DCF0BA20CB0E14BAA15_CustomAttributesCacheGenerator_U3CComeBackU3Ed__7_System_IDisposable_Dispose_m6EC106E82E8310369A4E6F2C120508D52A7373F6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CComeBackU3Ed__7_t85EB3F03ABCCF46458945DCF0BA20CB0E14BAA15_CustomAttributesCacheGenerator_U3CComeBackU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m56B30F705652C33EBE7460DA102F14C46CDDD025(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CComeBackU3Ed__7_t85EB3F03ABCCF46458945DCF0BA20CB0E14BAA15_CustomAttributesCacheGenerator_U3CComeBackU3Ed__7_System_Collections_IEnumerator_Reset_mFC6E53B7484FB463E3CBEE2D7343C4FE8CCF97AA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CComeBackU3Ed__7_t85EB3F03ABCCF46458945DCF0BA20CB0E14BAA15_CustomAttributesCacheGenerator_U3CComeBackU3Ed__7_System_Collections_IEnumerator_get_Current_m267AE7046310A522766FD54D017626A2A2158113(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Dialogue_tA1C94EA0CB09E76D320538CBB50740460F5D15F3_CustomAttributesCacheGenerator_Dialogue_Type_m0971D31541433FE207337205D7BBD86A86D553A1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTypeU3Ed__9_tC61615E885ED8F22B60E31A1D10446F01FEC8C51_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTypeU3Ed__9_tC61615E885ED8F22B60E31A1D10446F01FEC8C51_0_0_0_var), NULL);
	}
}
static void U3CTypeU3Ed__9_tC61615E885ED8F22B60E31A1D10446F01FEC8C51_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTypeU3Ed__9_tC61615E885ED8F22B60E31A1D10446F01FEC8C51_CustomAttributesCacheGenerator_U3CTypeU3Ed__9__ctor_m1B72D019C1EA5A0A2173E7E76DA67C90D52BF057(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeU3Ed__9_tC61615E885ED8F22B60E31A1D10446F01FEC8C51_CustomAttributesCacheGenerator_U3CTypeU3Ed__9_System_IDisposable_Dispose_m541ACFCBEAE51731393C3D48A9F5BBD33EFA9150(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeU3Ed__9_tC61615E885ED8F22B60E31A1D10446F01FEC8C51_CustomAttributesCacheGenerator_U3CTypeU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4A03A88D3AE993822722B3F4C124939AF7A1E2CB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeU3Ed__9_tC61615E885ED8F22B60E31A1D10446F01FEC8C51_CustomAttributesCacheGenerator_U3CTypeU3Ed__9_System_Collections_IEnumerator_Reset_mD42721D7BC51BA10692735533B8F397787C0C415(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeU3Ed__9_tC61615E885ED8F22B60E31A1D10446F01FEC8C51_CustomAttributesCacheGenerator_U3CTypeU3Ed__9_System_Collections_IEnumerator_get_Current_m745F9253F292285753FE51DEF39C7C36F1062123(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void FriendScript_tE93FC3609E2D8DE20291991CA8BBB6C6DAA522D3_CustomAttributesCacheGenerator_FriendScript_ComeBack_m9D5A66FD7D03E835C0BA950C267F269BCE53C03B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CComeBackU3Ed__14_t8D490D27296D92B089F1735C495908DCC4CE24A5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CComeBackU3Ed__14_t8D490D27296D92B089F1735C495908DCC4CE24A5_0_0_0_var), NULL);
	}
}
static void U3CComeBackU3Ed__14_t8D490D27296D92B089F1735C495908DCC4CE24A5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CComeBackU3Ed__14_t8D490D27296D92B089F1735C495908DCC4CE24A5_CustomAttributesCacheGenerator_U3CComeBackU3Ed__14__ctor_mC74EA0807A6E158BC6A9666B670D2A97ED65F997(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CComeBackU3Ed__14_t8D490D27296D92B089F1735C495908DCC4CE24A5_CustomAttributesCacheGenerator_U3CComeBackU3Ed__14_System_IDisposable_Dispose_m31F49FEFDB2165C05240B59110111A1365613DDB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CComeBackU3Ed__14_t8D490D27296D92B089F1735C495908DCC4CE24A5_CustomAttributesCacheGenerator_U3CComeBackU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m13C8C6D21FB3E83D188C00D3BD1F5B6FA07FE2AA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CComeBackU3Ed__14_t8D490D27296D92B089F1735C495908DCC4CE24A5_CustomAttributesCacheGenerator_U3CComeBackU3Ed__14_System_Collections_IEnumerator_Reset_m40B4F07E1E4485ED69988DEDED302A2CE03EBC49(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CComeBackU3Ed__14_t8D490D27296D92B089F1735C495908DCC4CE24A5_CustomAttributesCacheGenerator_U3CComeBackU3Ed__14_System_Collections_IEnumerator_get_Current_m23B5B0D5D97C8425C816649667DC939D72E8DC41(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ProfEvent_t4EA516F4D9511661256D0723A7CE648F8446DD88_CustomAttributesCacheGenerator_ProfEvent_Dilkmore_m3E3586F9AB818095499330D0E8CF9921E5B7E231(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDilkmoreU3Ed__26_tD14549EAB7016DB0FF928B2F9AE2F2CFD2ED3EBA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDilkmoreU3Ed__26_tD14549EAB7016DB0FF928B2F9AE2F2CFD2ED3EBA_0_0_0_var), NULL);
	}
}
static void ProfEvent_t4EA516F4D9511661256D0723A7CE648F8446DD88_CustomAttributesCacheGenerator_ProfEvent_Wegener_m809B72CC414FB7ADE6F724814BD78FDE6821D916(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWegenerU3Ed__27_t3927F04344617AE8A5AD0A5D83C4FE88A17A3A46_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWegenerU3Ed__27_t3927F04344617AE8A5AD0A5D83C4FE88A17A3A46_0_0_0_var), NULL);
	}
}
static void ProfEvent_t4EA516F4D9511661256D0723A7CE648F8446DD88_CustomAttributesCacheGenerator_ProfEvent_Galifianakis_m1A941B09414BE7041F4CF1A90B0DC6FF7495B446(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGalifianakisU3Ed__28_t880F19F9E66C55B2EEB9F5E5C2A64562596E98C2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGalifianakisU3Ed__28_t880F19F9E66C55B2EEB9F5E5C2A64562596E98C2_0_0_0_var), NULL);
	}
}
static void ProfEvent_t4EA516F4D9511661256D0723A7CE648F8446DD88_CustomAttributesCacheGenerator_ProfEvent_OpeningSound_mA1CF54B9E12A5FC4C0ED7A6C728A6DDFA09EE339(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COpeningSoundU3Ed__29_tB3097080664383B3EAD4A1C113AD9682FDC6904D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3COpeningSoundU3Ed__29_tB3097080664383B3EAD4A1C113AD9682FDC6904D_0_0_0_var), NULL);
	}
}
static void U3CDilkmoreU3Ed__26_tD14549EAB7016DB0FF928B2F9AE2F2CFD2ED3EBA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDilkmoreU3Ed__26_tD14549EAB7016DB0FF928B2F9AE2F2CFD2ED3EBA_CustomAttributesCacheGenerator_U3CDilkmoreU3Ed__26__ctor_m74A78C78F1DCC68043ED881687093E11A1B175FB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDilkmoreU3Ed__26_tD14549EAB7016DB0FF928B2F9AE2F2CFD2ED3EBA_CustomAttributesCacheGenerator_U3CDilkmoreU3Ed__26_System_IDisposable_Dispose_m7188F479FF8C8E7688B8B308F65F84F30CD0072D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDilkmoreU3Ed__26_tD14549EAB7016DB0FF928B2F9AE2F2CFD2ED3EBA_CustomAttributesCacheGenerator_U3CDilkmoreU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4DAB88BCFCBC8944428DFFD9160DFD746C925248(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDilkmoreU3Ed__26_tD14549EAB7016DB0FF928B2F9AE2F2CFD2ED3EBA_CustomAttributesCacheGenerator_U3CDilkmoreU3Ed__26_System_Collections_IEnumerator_Reset_mFC03480CC6EF6321372F4D0112FF095510A93E94(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDilkmoreU3Ed__26_tD14549EAB7016DB0FF928B2F9AE2F2CFD2ED3EBA_CustomAttributesCacheGenerator_U3CDilkmoreU3Ed__26_System_Collections_IEnumerator_get_Current_m163696E72D0ADCBF7DB7D315AB46CFC2C10F2DD8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWegenerU3Ed__27_t3927F04344617AE8A5AD0A5D83C4FE88A17A3A46_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWegenerU3Ed__27_t3927F04344617AE8A5AD0A5D83C4FE88A17A3A46_CustomAttributesCacheGenerator_U3CWegenerU3Ed__27__ctor_m884191B8383FF0E2D118C0BEE467D4222A088153(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWegenerU3Ed__27_t3927F04344617AE8A5AD0A5D83C4FE88A17A3A46_CustomAttributesCacheGenerator_U3CWegenerU3Ed__27_System_IDisposable_Dispose_m762CD4601F4060751E62034D8B14002B2C0A674F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWegenerU3Ed__27_t3927F04344617AE8A5AD0A5D83C4FE88A17A3A46_CustomAttributesCacheGenerator_U3CWegenerU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB4FC103DFE38FBACCD31A8DCE976328EF91561AF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWegenerU3Ed__27_t3927F04344617AE8A5AD0A5D83C4FE88A17A3A46_CustomAttributesCacheGenerator_U3CWegenerU3Ed__27_System_Collections_IEnumerator_Reset_m5CC5F42BE61899FDEB9A6CF2BA6350C2D0527AF4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWegenerU3Ed__27_t3927F04344617AE8A5AD0A5D83C4FE88A17A3A46_CustomAttributesCacheGenerator_U3CWegenerU3Ed__27_System_Collections_IEnumerator_get_Current_m1766E7456795CD3A55099FAA257A3F8653B6A85C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGalifianakisU3Ed__28_t880F19F9E66C55B2EEB9F5E5C2A64562596E98C2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGalifianakisU3Ed__28_t880F19F9E66C55B2EEB9F5E5C2A64562596E98C2_CustomAttributesCacheGenerator_U3CGalifianakisU3Ed__28__ctor_m0ED258D08EA42FA5E3A95691C4EC5CF993CAB6E8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGalifianakisU3Ed__28_t880F19F9E66C55B2EEB9F5E5C2A64562596E98C2_CustomAttributesCacheGenerator_U3CGalifianakisU3Ed__28_System_IDisposable_Dispose_mDCBC4062F51871556F133C8CCCFB3CD1DF83333E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGalifianakisU3Ed__28_t880F19F9E66C55B2EEB9F5E5C2A64562596E98C2_CustomAttributesCacheGenerator_U3CGalifianakisU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8BB89622EE3E1D42F3EADDD6ED5EE76CA9499FA9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGalifianakisU3Ed__28_t880F19F9E66C55B2EEB9F5E5C2A64562596E98C2_CustomAttributesCacheGenerator_U3CGalifianakisU3Ed__28_System_Collections_IEnumerator_Reset_mC88D506B123BABF84B47AFB3879DE29F4B679CEB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGalifianakisU3Ed__28_t880F19F9E66C55B2EEB9F5E5C2A64562596E98C2_CustomAttributesCacheGenerator_U3CGalifianakisU3Ed__28_System_Collections_IEnumerator_get_Current_m92BC5B93CBB500C8CCE133665A64DE3167CBE93C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpeningSoundU3Ed__29_tB3097080664383B3EAD4A1C113AD9682FDC6904D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COpeningSoundU3Ed__29_tB3097080664383B3EAD4A1C113AD9682FDC6904D_CustomAttributesCacheGenerator_U3COpeningSoundU3Ed__29__ctor_m1AB01C927F90ED00CFAE8A5C82158EB001A427CF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpeningSoundU3Ed__29_tB3097080664383B3EAD4A1C113AD9682FDC6904D_CustomAttributesCacheGenerator_U3COpeningSoundU3Ed__29_System_IDisposable_Dispose_m8E717215B45659AAB3538D6CABD11AD3A3C9AE3C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpeningSoundU3Ed__29_tB3097080664383B3EAD4A1C113AD9682FDC6904D_CustomAttributesCacheGenerator_U3COpeningSoundU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14E182718730081CD4EF60622DCD53463C23E56E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpeningSoundU3Ed__29_tB3097080664383B3EAD4A1C113AD9682FDC6904D_CustomAttributesCacheGenerator_U3COpeningSoundU3Ed__29_System_Collections_IEnumerator_Reset_mBFC669D77050B079430CCDF52C0F29E4F42ADB44(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpeningSoundU3Ed__29_tB3097080664383B3EAD4A1C113AD9682FDC6904D_CustomAttributesCacheGenerator_U3COpeningSoundU3Ed__29_System_Collections_IEnumerator_get_Current_mF6ED367165D4B150DA78DF88E0EE8C2ECAB15088(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Refridgerator_t33C30549B0D0DCA5640AB17F2551E05288E64A60_CustomAttributesCacheGenerator_Refridgerator_CloseFridge_mECB049F0FD280908EE24909A7D19FCD2E0D4E97C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCloseFridgeU3Ed__20_t1475090F6F547C8F6CC74550993C1E1A8EECB28A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCloseFridgeU3Ed__20_t1475090F6F547C8F6CC74550993C1E1A8EECB28A_0_0_0_var), NULL);
	}
}
static void U3CCloseFridgeU3Ed__20_t1475090F6F547C8F6CC74550993C1E1A8EECB28A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCloseFridgeU3Ed__20_t1475090F6F547C8F6CC74550993C1E1A8EECB28A_CustomAttributesCacheGenerator_U3CCloseFridgeU3Ed__20__ctor_m91CE310CFA79F8474FB983A760A1635DF9DA4676(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCloseFridgeU3Ed__20_t1475090F6F547C8F6CC74550993C1E1A8EECB28A_CustomAttributesCacheGenerator_U3CCloseFridgeU3Ed__20_System_IDisposable_Dispose_m8805BCEBB6B944DB2965E8D157A958A39F50A028(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCloseFridgeU3Ed__20_t1475090F6F547C8F6CC74550993C1E1A8EECB28A_CustomAttributesCacheGenerator_U3CCloseFridgeU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC737CD2B69C6BE32344F591BFF2703C1EA74961B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCloseFridgeU3Ed__20_t1475090F6F547C8F6CC74550993C1E1A8EECB28A_CustomAttributesCacheGenerator_U3CCloseFridgeU3Ed__20_System_Collections_IEnumerator_Reset_mA84DA17A6D3E0647A7BFAC0AA8C102D518F3B578(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCloseFridgeU3Ed__20_t1475090F6F547C8F6CC74550993C1E1A8EECB28A_CustomAttributesCacheGenerator_U3CCloseFridgeU3Ed__20_System_Collections_IEnumerator_get_Current_m768F475AF4DA707C96A4C1B1CB547CCFE3065B09(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[71] = 
{
	U3CGameOverSequenceU3Ed__38_t96FD66B07CFB03D6D8DDE3FF203D6BD2CB139D04_CustomAttributesCacheGenerator,
	U3CScoreSequenceU3Ed__39_t58C3F4AE9ADE5E40AC91712BA4E5C611E6CC9131_CustomAttributesCacheGenerator,
	U3CComeBackU3Ed__7_t85EB3F03ABCCF46458945DCF0BA20CB0E14BAA15_CustomAttributesCacheGenerator,
	U3CTypeU3Ed__9_tC61615E885ED8F22B60E31A1D10446F01FEC8C51_CustomAttributesCacheGenerator,
	U3CComeBackU3Ed__14_t8D490D27296D92B089F1735C495908DCC4CE24A5_CustomAttributesCacheGenerator,
	U3CDilkmoreU3Ed__26_tD14549EAB7016DB0FF928B2F9AE2F2CFD2ED3EBA_CustomAttributesCacheGenerator,
	U3CWegenerU3Ed__27_t3927F04344617AE8A5AD0A5D83C4FE88A17A3A46_CustomAttributesCacheGenerator,
	U3CGalifianakisU3Ed__28_t880F19F9E66C55B2EEB9F5E5C2A64562596E98C2_CustomAttributesCacheGenerator,
	U3COpeningSoundU3Ed__29_tB3097080664383B3EAD4A1C113AD9682FDC6904D_CustomAttributesCacheGenerator,
	U3CCloseFridgeU3Ed__20_t1475090F6F547C8F6CC74550993C1E1A8EECB28A_CustomAttributesCacheGenerator,
	Assignment_t2EA4CB9A2262E4E8F506A57FDE78131DDB71B2C2_CustomAttributesCacheGenerator_Assignment_GameOverSequence_m053B41B7BE1640A478893749BF18277856B18E56,
	Assignment_t2EA4CB9A2262E4E8F506A57FDE78131DDB71B2C2_CustomAttributesCacheGenerator_Assignment_ScoreSequence_m108330CC445851E85677F49CA88015EFF056C414,
	U3CGameOverSequenceU3Ed__38_t96FD66B07CFB03D6D8DDE3FF203D6BD2CB139D04_CustomAttributesCacheGenerator_U3CGameOverSequenceU3Ed__38__ctor_m2D5D76631A634FE147DB4F41DD4A0F5265ED58A8,
	U3CGameOverSequenceU3Ed__38_t96FD66B07CFB03D6D8DDE3FF203D6BD2CB139D04_CustomAttributesCacheGenerator_U3CGameOverSequenceU3Ed__38_System_IDisposable_Dispose_mEE520F987D74AE398C332F13FD99879065A00013,
	U3CGameOverSequenceU3Ed__38_t96FD66B07CFB03D6D8DDE3FF203D6BD2CB139D04_CustomAttributesCacheGenerator_U3CGameOverSequenceU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7D28F2C1D6EF1970DD18A275BAEE3E1EFC1C3129,
	U3CGameOverSequenceU3Ed__38_t96FD66B07CFB03D6D8DDE3FF203D6BD2CB139D04_CustomAttributesCacheGenerator_U3CGameOverSequenceU3Ed__38_System_Collections_IEnumerator_Reset_m1FD62067D3185FE927F9BF7639452F1851329C41,
	U3CGameOverSequenceU3Ed__38_t96FD66B07CFB03D6D8DDE3FF203D6BD2CB139D04_CustomAttributesCacheGenerator_U3CGameOverSequenceU3Ed__38_System_Collections_IEnumerator_get_Current_m185FBBE6329ACE7391C7E879B66FDCB986CD3F77,
	U3CScoreSequenceU3Ed__39_t58C3F4AE9ADE5E40AC91712BA4E5C611E6CC9131_CustomAttributesCacheGenerator_U3CScoreSequenceU3Ed__39__ctor_mAEF6264729299089B2280586619F541344088D82,
	U3CScoreSequenceU3Ed__39_t58C3F4AE9ADE5E40AC91712BA4E5C611E6CC9131_CustomAttributesCacheGenerator_U3CScoreSequenceU3Ed__39_System_IDisposable_Dispose_m3A87C2378875C252AD099024C4A3E0C12C6EF8C8,
	U3CScoreSequenceU3Ed__39_t58C3F4AE9ADE5E40AC91712BA4E5C611E6CC9131_CustomAttributesCacheGenerator_U3CScoreSequenceU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0D41F2085C7F0D9EFF41E229CE747AF7C8B045CE,
	U3CScoreSequenceU3Ed__39_t58C3F4AE9ADE5E40AC91712BA4E5C611E6CC9131_CustomAttributesCacheGenerator_U3CScoreSequenceU3Ed__39_System_Collections_IEnumerator_Reset_mB02F97C1ED30CDB2D4292D0210380CA4933A3360,
	U3CScoreSequenceU3Ed__39_t58C3F4AE9ADE5E40AC91712BA4E5C611E6CC9131_CustomAttributesCacheGenerator_U3CScoreSequenceU3Ed__39_System_Collections_IEnumerator_get_Current_m5518076540D1507B7F206374F1CA5BE66A9ADA62,
	BuyingFood_t973EF70C1C16785D647B36B3C7B24B6073345260_CustomAttributesCacheGenerator_BuyingFood_ComeBack_m194C119F380AE46C737A355B4452FA7FF25490F6,
	U3CComeBackU3Ed__7_t85EB3F03ABCCF46458945DCF0BA20CB0E14BAA15_CustomAttributesCacheGenerator_U3CComeBackU3Ed__7__ctor_m28EE7E5BAF8359271F86C22BE628B4B9FF2CF186,
	U3CComeBackU3Ed__7_t85EB3F03ABCCF46458945DCF0BA20CB0E14BAA15_CustomAttributesCacheGenerator_U3CComeBackU3Ed__7_System_IDisposable_Dispose_m6EC106E82E8310369A4E6F2C120508D52A7373F6,
	U3CComeBackU3Ed__7_t85EB3F03ABCCF46458945DCF0BA20CB0E14BAA15_CustomAttributesCacheGenerator_U3CComeBackU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m56B30F705652C33EBE7460DA102F14C46CDDD025,
	U3CComeBackU3Ed__7_t85EB3F03ABCCF46458945DCF0BA20CB0E14BAA15_CustomAttributesCacheGenerator_U3CComeBackU3Ed__7_System_Collections_IEnumerator_Reset_mFC6E53B7484FB463E3CBEE2D7343C4FE8CCF97AA,
	U3CComeBackU3Ed__7_t85EB3F03ABCCF46458945DCF0BA20CB0E14BAA15_CustomAttributesCacheGenerator_U3CComeBackU3Ed__7_System_Collections_IEnumerator_get_Current_m267AE7046310A522766FD54D017626A2A2158113,
	Dialogue_tA1C94EA0CB09E76D320538CBB50740460F5D15F3_CustomAttributesCacheGenerator_Dialogue_Type_m0971D31541433FE207337205D7BBD86A86D553A1,
	U3CTypeU3Ed__9_tC61615E885ED8F22B60E31A1D10446F01FEC8C51_CustomAttributesCacheGenerator_U3CTypeU3Ed__9__ctor_m1B72D019C1EA5A0A2173E7E76DA67C90D52BF057,
	U3CTypeU3Ed__9_tC61615E885ED8F22B60E31A1D10446F01FEC8C51_CustomAttributesCacheGenerator_U3CTypeU3Ed__9_System_IDisposable_Dispose_m541ACFCBEAE51731393C3D48A9F5BBD33EFA9150,
	U3CTypeU3Ed__9_tC61615E885ED8F22B60E31A1D10446F01FEC8C51_CustomAttributesCacheGenerator_U3CTypeU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4A03A88D3AE993822722B3F4C124939AF7A1E2CB,
	U3CTypeU3Ed__9_tC61615E885ED8F22B60E31A1D10446F01FEC8C51_CustomAttributesCacheGenerator_U3CTypeU3Ed__9_System_Collections_IEnumerator_Reset_mD42721D7BC51BA10692735533B8F397787C0C415,
	U3CTypeU3Ed__9_tC61615E885ED8F22B60E31A1D10446F01FEC8C51_CustomAttributesCacheGenerator_U3CTypeU3Ed__9_System_Collections_IEnumerator_get_Current_m745F9253F292285753FE51DEF39C7C36F1062123,
	FriendScript_tE93FC3609E2D8DE20291991CA8BBB6C6DAA522D3_CustomAttributesCacheGenerator_FriendScript_ComeBack_m9D5A66FD7D03E835C0BA950C267F269BCE53C03B,
	U3CComeBackU3Ed__14_t8D490D27296D92B089F1735C495908DCC4CE24A5_CustomAttributesCacheGenerator_U3CComeBackU3Ed__14__ctor_mC74EA0807A6E158BC6A9666B670D2A97ED65F997,
	U3CComeBackU3Ed__14_t8D490D27296D92B089F1735C495908DCC4CE24A5_CustomAttributesCacheGenerator_U3CComeBackU3Ed__14_System_IDisposable_Dispose_m31F49FEFDB2165C05240B59110111A1365613DDB,
	U3CComeBackU3Ed__14_t8D490D27296D92B089F1735C495908DCC4CE24A5_CustomAttributesCacheGenerator_U3CComeBackU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m13C8C6D21FB3E83D188C00D3BD1F5B6FA07FE2AA,
	U3CComeBackU3Ed__14_t8D490D27296D92B089F1735C495908DCC4CE24A5_CustomAttributesCacheGenerator_U3CComeBackU3Ed__14_System_Collections_IEnumerator_Reset_m40B4F07E1E4485ED69988DEDED302A2CE03EBC49,
	U3CComeBackU3Ed__14_t8D490D27296D92B089F1735C495908DCC4CE24A5_CustomAttributesCacheGenerator_U3CComeBackU3Ed__14_System_Collections_IEnumerator_get_Current_m23B5B0D5D97C8425C816649667DC939D72E8DC41,
	ProfEvent_t4EA516F4D9511661256D0723A7CE648F8446DD88_CustomAttributesCacheGenerator_ProfEvent_Dilkmore_m3E3586F9AB818095499330D0E8CF9921E5B7E231,
	ProfEvent_t4EA516F4D9511661256D0723A7CE648F8446DD88_CustomAttributesCacheGenerator_ProfEvent_Wegener_m809B72CC414FB7ADE6F724814BD78FDE6821D916,
	ProfEvent_t4EA516F4D9511661256D0723A7CE648F8446DD88_CustomAttributesCacheGenerator_ProfEvent_Galifianakis_m1A941B09414BE7041F4CF1A90B0DC6FF7495B446,
	ProfEvent_t4EA516F4D9511661256D0723A7CE648F8446DD88_CustomAttributesCacheGenerator_ProfEvent_OpeningSound_mA1CF54B9E12A5FC4C0ED7A6C728A6DDFA09EE339,
	U3CDilkmoreU3Ed__26_tD14549EAB7016DB0FF928B2F9AE2F2CFD2ED3EBA_CustomAttributesCacheGenerator_U3CDilkmoreU3Ed__26__ctor_m74A78C78F1DCC68043ED881687093E11A1B175FB,
	U3CDilkmoreU3Ed__26_tD14549EAB7016DB0FF928B2F9AE2F2CFD2ED3EBA_CustomAttributesCacheGenerator_U3CDilkmoreU3Ed__26_System_IDisposable_Dispose_m7188F479FF8C8E7688B8B308F65F84F30CD0072D,
	U3CDilkmoreU3Ed__26_tD14549EAB7016DB0FF928B2F9AE2F2CFD2ED3EBA_CustomAttributesCacheGenerator_U3CDilkmoreU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4DAB88BCFCBC8944428DFFD9160DFD746C925248,
	U3CDilkmoreU3Ed__26_tD14549EAB7016DB0FF928B2F9AE2F2CFD2ED3EBA_CustomAttributesCacheGenerator_U3CDilkmoreU3Ed__26_System_Collections_IEnumerator_Reset_mFC03480CC6EF6321372F4D0112FF095510A93E94,
	U3CDilkmoreU3Ed__26_tD14549EAB7016DB0FF928B2F9AE2F2CFD2ED3EBA_CustomAttributesCacheGenerator_U3CDilkmoreU3Ed__26_System_Collections_IEnumerator_get_Current_m163696E72D0ADCBF7DB7D315AB46CFC2C10F2DD8,
	U3CWegenerU3Ed__27_t3927F04344617AE8A5AD0A5D83C4FE88A17A3A46_CustomAttributesCacheGenerator_U3CWegenerU3Ed__27__ctor_m884191B8383FF0E2D118C0BEE467D4222A088153,
	U3CWegenerU3Ed__27_t3927F04344617AE8A5AD0A5D83C4FE88A17A3A46_CustomAttributesCacheGenerator_U3CWegenerU3Ed__27_System_IDisposable_Dispose_m762CD4601F4060751E62034D8B14002B2C0A674F,
	U3CWegenerU3Ed__27_t3927F04344617AE8A5AD0A5D83C4FE88A17A3A46_CustomAttributesCacheGenerator_U3CWegenerU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB4FC103DFE38FBACCD31A8DCE976328EF91561AF,
	U3CWegenerU3Ed__27_t3927F04344617AE8A5AD0A5D83C4FE88A17A3A46_CustomAttributesCacheGenerator_U3CWegenerU3Ed__27_System_Collections_IEnumerator_Reset_m5CC5F42BE61899FDEB9A6CF2BA6350C2D0527AF4,
	U3CWegenerU3Ed__27_t3927F04344617AE8A5AD0A5D83C4FE88A17A3A46_CustomAttributesCacheGenerator_U3CWegenerU3Ed__27_System_Collections_IEnumerator_get_Current_m1766E7456795CD3A55099FAA257A3F8653B6A85C,
	U3CGalifianakisU3Ed__28_t880F19F9E66C55B2EEB9F5E5C2A64562596E98C2_CustomAttributesCacheGenerator_U3CGalifianakisU3Ed__28__ctor_m0ED258D08EA42FA5E3A95691C4EC5CF993CAB6E8,
	U3CGalifianakisU3Ed__28_t880F19F9E66C55B2EEB9F5E5C2A64562596E98C2_CustomAttributesCacheGenerator_U3CGalifianakisU3Ed__28_System_IDisposable_Dispose_mDCBC4062F51871556F133C8CCCFB3CD1DF83333E,
	U3CGalifianakisU3Ed__28_t880F19F9E66C55B2EEB9F5E5C2A64562596E98C2_CustomAttributesCacheGenerator_U3CGalifianakisU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8BB89622EE3E1D42F3EADDD6ED5EE76CA9499FA9,
	U3CGalifianakisU3Ed__28_t880F19F9E66C55B2EEB9F5E5C2A64562596E98C2_CustomAttributesCacheGenerator_U3CGalifianakisU3Ed__28_System_Collections_IEnumerator_Reset_mC88D506B123BABF84B47AFB3879DE29F4B679CEB,
	U3CGalifianakisU3Ed__28_t880F19F9E66C55B2EEB9F5E5C2A64562596E98C2_CustomAttributesCacheGenerator_U3CGalifianakisU3Ed__28_System_Collections_IEnumerator_get_Current_m92BC5B93CBB500C8CCE133665A64DE3167CBE93C,
	U3COpeningSoundU3Ed__29_tB3097080664383B3EAD4A1C113AD9682FDC6904D_CustomAttributesCacheGenerator_U3COpeningSoundU3Ed__29__ctor_m1AB01C927F90ED00CFAE8A5C82158EB001A427CF,
	U3COpeningSoundU3Ed__29_tB3097080664383B3EAD4A1C113AD9682FDC6904D_CustomAttributesCacheGenerator_U3COpeningSoundU3Ed__29_System_IDisposable_Dispose_m8E717215B45659AAB3538D6CABD11AD3A3C9AE3C,
	U3COpeningSoundU3Ed__29_tB3097080664383B3EAD4A1C113AD9682FDC6904D_CustomAttributesCacheGenerator_U3COpeningSoundU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14E182718730081CD4EF60622DCD53463C23E56E,
	U3COpeningSoundU3Ed__29_tB3097080664383B3EAD4A1C113AD9682FDC6904D_CustomAttributesCacheGenerator_U3COpeningSoundU3Ed__29_System_Collections_IEnumerator_Reset_mBFC669D77050B079430CCDF52C0F29E4F42ADB44,
	U3COpeningSoundU3Ed__29_tB3097080664383B3EAD4A1C113AD9682FDC6904D_CustomAttributesCacheGenerator_U3COpeningSoundU3Ed__29_System_Collections_IEnumerator_get_Current_mF6ED367165D4B150DA78DF88E0EE8C2ECAB15088,
	Refridgerator_t33C30549B0D0DCA5640AB17F2551E05288E64A60_CustomAttributesCacheGenerator_Refridgerator_CloseFridge_mECB049F0FD280908EE24909A7D19FCD2E0D4E97C,
	U3CCloseFridgeU3Ed__20_t1475090F6F547C8F6CC74550993C1E1A8EECB28A_CustomAttributesCacheGenerator_U3CCloseFridgeU3Ed__20__ctor_m91CE310CFA79F8474FB983A760A1635DF9DA4676,
	U3CCloseFridgeU3Ed__20_t1475090F6F547C8F6CC74550993C1E1A8EECB28A_CustomAttributesCacheGenerator_U3CCloseFridgeU3Ed__20_System_IDisposable_Dispose_m8805BCEBB6B944DB2965E8D157A958A39F50A028,
	U3CCloseFridgeU3Ed__20_t1475090F6F547C8F6CC74550993C1E1A8EECB28A_CustomAttributesCacheGenerator_U3CCloseFridgeU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC737CD2B69C6BE32344F591BFF2703C1EA74961B,
	U3CCloseFridgeU3Ed__20_t1475090F6F547C8F6CC74550993C1E1A8EECB28A_CustomAttributesCacheGenerator_U3CCloseFridgeU3Ed__20_System_Collections_IEnumerator_Reset_mA84DA17A6D3E0647A7BFAC0AA8C102D518F3B578,
	U3CCloseFridgeU3Ed__20_t1475090F6F547C8F6CC74550993C1E1A8EECB28A_CustomAttributesCacheGenerator_U3CCloseFridgeU3Ed__20_System_Collections_IEnumerator_get_Current_m768F475AF4DA707C96A4C1B1CB547CCFE3065B09,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
