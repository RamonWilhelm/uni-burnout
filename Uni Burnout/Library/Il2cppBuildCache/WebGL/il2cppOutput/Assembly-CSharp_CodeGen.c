﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Assignment::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Assignment_OnTriggerEnter2D_m69634A262155CB54A55FF6E73D6805A6E9537996 (void);
// 0x00000002 System.Void Assignment::OnTriggerExit2D(UnityEngine.Collider2D)
extern void Assignment_OnTriggerExit2D_m9BA562CF00E41CBFBB3BFA469562EC423AC12B07 (void);
// 0x00000003 System.Void Assignment::Start()
extern void Assignment_Start_m036CD2F2E014EC7C1C39D548801D6BE681C8F625 (void);
// 0x00000004 System.Void Assignment::Update()
extern void Assignment_Update_mA52D445775AA9A260861FFF4058A1B2FAC88D80A (void);
// 0x00000005 System.Collections.IEnumerator Assignment::GameOverSequence()
extern void Assignment_GameOverSequence_m053B41B7BE1640A478893749BF18277856B18E56 (void);
// 0x00000006 System.Collections.IEnumerator Assignment::ScoreSequence()
extern void Assignment_ScoreSequence_m108330CC445851E85677F49CA88015EFF056C414 (void);
// 0x00000007 System.Void Assignment::resetStatics()
extern void Assignment_resetStatics_mA7F7163D23E442C22A5786599289030BEDA36E55 (void);
// 0x00000008 System.Void Assignment::.ctor()
extern void Assignment__ctor_m33010A19C5CC18368E18FA9232A9F586EAE37B4A (void);
// 0x00000009 System.Void Assignment::.cctor()
extern void Assignment__cctor_m4917921C151EF2963E230CAE2EDD23038EB66DB4 (void);
// 0x0000000A System.Void Assignment/<GameOverSequence>d__38::.ctor(System.Int32)
extern void U3CGameOverSequenceU3Ed__38__ctor_m2D5D76631A634FE147DB4F41DD4A0F5265ED58A8 (void);
// 0x0000000B System.Void Assignment/<GameOverSequence>d__38::System.IDisposable.Dispose()
extern void U3CGameOverSequenceU3Ed__38_System_IDisposable_Dispose_mEE520F987D74AE398C332F13FD99879065A00013 (void);
// 0x0000000C System.Boolean Assignment/<GameOverSequence>d__38::MoveNext()
extern void U3CGameOverSequenceU3Ed__38_MoveNext_m6F8FC1C8E36AAD3D901E5F4DEED0519E54488894 (void);
// 0x0000000D System.Object Assignment/<GameOverSequence>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGameOverSequenceU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7D28F2C1D6EF1970DD18A275BAEE3E1EFC1C3129 (void);
// 0x0000000E System.Void Assignment/<GameOverSequence>d__38::System.Collections.IEnumerator.Reset()
extern void U3CGameOverSequenceU3Ed__38_System_Collections_IEnumerator_Reset_m1FD62067D3185FE927F9BF7639452F1851329C41 (void);
// 0x0000000F System.Object Assignment/<GameOverSequence>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CGameOverSequenceU3Ed__38_System_Collections_IEnumerator_get_Current_m185FBBE6329ACE7391C7E879B66FDCB986CD3F77 (void);
// 0x00000010 System.Void Assignment/<ScoreSequence>d__39::.ctor(System.Int32)
extern void U3CScoreSequenceU3Ed__39__ctor_mAEF6264729299089B2280586619F541344088D82 (void);
// 0x00000011 System.Void Assignment/<ScoreSequence>d__39::System.IDisposable.Dispose()
extern void U3CScoreSequenceU3Ed__39_System_IDisposable_Dispose_m3A87C2378875C252AD099024C4A3E0C12C6EF8C8 (void);
// 0x00000012 System.Boolean Assignment/<ScoreSequence>d__39::MoveNext()
extern void U3CScoreSequenceU3Ed__39_MoveNext_mEA3DE579B3993C478BD63AB655EF431D98B66557 (void);
// 0x00000013 System.Object Assignment/<ScoreSequence>d__39::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CScoreSequenceU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0D41F2085C7F0D9EFF41E229CE747AF7C8B045CE (void);
// 0x00000014 System.Void Assignment/<ScoreSequence>d__39::System.Collections.IEnumerator.Reset()
extern void U3CScoreSequenceU3Ed__39_System_Collections_IEnumerator_Reset_mB02F97C1ED30CDB2D4292D0210380CA4933A3360 (void);
// 0x00000015 System.Object Assignment/<ScoreSequence>d__39::System.Collections.IEnumerator.get_Current()
extern void U3CScoreSequenceU3Ed__39_System_Collections_IEnumerator_get_Current_m5518076540D1507B7F206374F1CA5BE66A9ADA62 (void);
// 0x00000016 System.Void BedScript::Start()
extern void BedScript_Start_mCDA7CFD534EC65CB57B3AE1169EDEB0C0951F230 (void);
// 0x00000017 System.Void BedScript::Update()
extern void BedScript_Update_m90C210DB03D228FEE76BDC3A964C635CEDBE10F2 (void);
// 0x00000018 System.Void BedScript::.ctor()
extern void BedScript__ctor_mD7CF14E6E61808E8D196E39670C031FB678490D2 (void);
// 0x00000019 System.Void BuyingFood::Start()
extern void BuyingFood_Start_m4F9236312F81E09E97DC02B3630D842D0CCA91F2 (void);
// 0x0000001A System.Void BuyingFood::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void BuyingFood_OnTriggerEnter2D_m98DDB7A0E376DC81681037FD30C00D778AF03174 (void);
// 0x0000001B System.Void BuyingFood::OnTriggerExit2D(UnityEngine.Collider2D)
extern void BuyingFood_OnTriggerExit2D_m21345A2424528E6F57C9E87CA10BC4A3263AD2B4 (void);
// 0x0000001C System.Void BuyingFood::Update()
extern void BuyingFood_Update_m3843AAC1AD292A2CFFDBD7361D138AC5D234B363 (void);
// 0x0000001D System.Collections.IEnumerator BuyingFood::ComeBack()
extern void BuyingFood_ComeBack_m194C119F380AE46C737A355B4452FA7FF25490F6 (void);
// 0x0000001E System.Void BuyingFood::.ctor()
extern void BuyingFood__ctor_mEADBB6BD9651F50F0CF9D6DAA8AE707A662D4053 (void);
// 0x0000001F System.Void BuyingFood/<ComeBack>d__7::.ctor(System.Int32)
extern void U3CComeBackU3Ed__7__ctor_m28EE7E5BAF8359271F86C22BE628B4B9FF2CF186 (void);
// 0x00000020 System.Void BuyingFood/<ComeBack>d__7::System.IDisposable.Dispose()
extern void U3CComeBackU3Ed__7_System_IDisposable_Dispose_m6EC106E82E8310369A4E6F2C120508D52A7373F6 (void);
// 0x00000021 System.Boolean BuyingFood/<ComeBack>d__7::MoveNext()
extern void U3CComeBackU3Ed__7_MoveNext_mEB04AD82D1BFF1FBA424A80EB5F560AAC0D02A99 (void);
// 0x00000022 System.Object BuyingFood/<ComeBack>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CComeBackU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m56B30F705652C33EBE7460DA102F14C46CDDD025 (void);
// 0x00000023 System.Void BuyingFood/<ComeBack>d__7::System.Collections.IEnumerator.Reset()
extern void U3CComeBackU3Ed__7_System_Collections_IEnumerator_Reset_mFC6E53B7484FB463E3CBEE2D7343C4FE8CCF97AA (void);
// 0x00000024 System.Object BuyingFood/<ComeBack>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CComeBackU3Ed__7_System_Collections_IEnumerator_get_Current_m267AE7046310A522766FD54D017626A2A2158113 (void);
// 0x00000025 System.Void ClockTime::Start()
extern void ClockTime_Start_mA429B55638ABE8DAEE2ECC5B8F3D2F88F6BAC72B (void);
// 0x00000026 System.Void ClockTime::Update()
extern void ClockTime_Update_mA09A1FC0D030C62047EBF2183AA40AEB0B7CF906 (void);
// 0x00000027 System.Void ClockTime::changeMinute()
extern void ClockTime_changeMinute_mBCF9C130D7B010F6D5A888882DD746FEF7237A65 (void);
// 0x00000028 System.Void ClockTime::changeHour()
extern void ClockTime_changeHour_mA2C2FE1A5A2DB7D2304B74EC54374E689A70461A (void);
// 0x00000029 System.Void ClockTime::.ctor()
extern void ClockTime__ctor_m931BA6807FE6950E4F243A6591AB9500A899C341 (void);
// 0x0000002A System.Void ClockTime::.cctor()
extern void ClockTime__cctor_m872BD54DCCC42A4FD7E5AD77FD391DA47F3D26DB (void);
// 0x0000002B System.Void CounterObject::Start()
extern void CounterObject_Start_m95ECC341D7486148CF08083C654182262FC53A3D (void);
// 0x0000002C System.Void CounterObject::Update()
extern void CounterObject_Update_mC77DACE177DA105B352D4F1E8077DC4C957DB46A (void);
// 0x0000002D System.Void CounterObject::.ctor()
extern void CounterObject__ctor_mCA1731E6FB39CEFF8A8AB9EEB50A151B3EC0F05B (void);
// 0x0000002E System.Void CounterObject::.cctor()
extern void CounterObject__cctor_m80451F82A64AD2A36F4EB787108A15CB3EF5670B (void);
// 0x0000002F System.Void Dialogue::Display(System.String,System.Single,System.Single)
extern void Dialogue_Display_m7A893184DF6C1DFE4B1C3AC35FA32EA48E14F897 (void);
// 0x00000030 System.Void Dialogue::StopTyping()
extern void Dialogue_StopTyping_mD671FAEE764B6E569E89C472DE3EF870C45BB9BE (void);
// 0x00000031 System.Collections.IEnumerator Dialogue::Type()
extern void Dialogue_Type_m0971D31541433FE207337205D7BBD86A86D553A1 (void);
// 0x00000032 System.Void Dialogue::.ctor()
extern void Dialogue__ctor_m76C11FD80AFD4F6BE87E30C737879EBB5A1D724E (void);
// 0x00000033 System.Void Dialogue/<Type>d__9::.ctor(System.Int32)
extern void U3CTypeU3Ed__9__ctor_m1B72D019C1EA5A0A2173E7E76DA67C90D52BF057 (void);
// 0x00000034 System.Void Dialogue/<Type>d__9::System.IDisposable.Dispose()
extern void U3CTypeU3Ed__9_System_IDisposable_Dispose_m541ACFCBEAE51731393C3D48A9F5BBD33EFA9150 (void);
// 0x00000035 System.Boolean Dialogue/<Type>d__9::MoveNext()
extern void U3CTypeU3Ed__9_MoveNext_m4A208C7998709D7740F470D11CE84EB8553349FD (void);
// 0x00000036 System.Object Dialogue/<Type>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTypeU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4A03A88D3AE993822722B3F4C124939AF7A1E2CB (void);
// 0x00000037 System.Void Dialogue/<Type>d__9::System.Collections.IEnumerator.Reset()
extern void U3CTypeU3Ed__9_System_Collections_IEnumerator_Reset_mD42721D7BC51BA10692735533B8F397787C0C415 (void);
// 0x00000038 System.Object Dialogue/<Type>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CTypeU3Ed__9_System_Collections_IEnumerator_get_Current_m745F9253F292285753FE51DEF39C7C36F1062123 (void);
// 0x00000039 System.Void DoorTrigger::Start()
extern void DoorTrigger_Start_m90DB9D0A9246E1EB09B43C23E4E329E8BE05BE7B (void);
// 0x0000003A System.Void DoorTrigger::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void DoorTrigger_OnTriggerEnter2D_m3A67FAC61DBB4FBDA085A13C27DEEA4C88A932F5 (void);
// 0x0000003B System.Void DoorTrigger::OnTriggerExit2D(UnityEngine.Collider2D)
extern void DoorTrigger_OnTriggerExit2D_mAB29C2222ED5A40BA25FFC194F08183184773132 (void);
// 0x0000003C System.Void DoorTrigger::Update()
extern void DoorTrigger_Update_m4E576A3AFB269F0B116ED353CA0FF1D19527F12E (void);
// 0x0000003D System.Void DoorTrigger::.ctor()
extern void DoorTrigger__ctor_m91AD0C323D39159697E05023696804017734CB6F (void);
// 0x0000003E System.Void FriendScript::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void FriendScript_OnTriggerEnter2D_m047B38AD226422941E349B4233668147D1EA4D3E (void);
// 0x0000003F System.Void FriendScript::OnTriggerExit2D(UnityEngine.Collider2D)
extern void FriendScript_OnTriggerExit2D_m941324B74AC497F7DB263D680AF46A7E97B8F1F0 (void);
// 0x00000040 System.Void FriendScript::Start()
extern void FriendScript_Start_m026F9A96E13DDB3296FA27366655ACD45C3774FB (void);
// 0x00000041 System.Void FriendScript::Update()
extern void FriendScript_Update_m880EE63CDC29B786C8EA59BD96B507408D5A836B (void);
// 0x00000042 System.Collections.IEnumerator FriendScript::ComeBack()
extern void FriendScript_ComeBack_m9D5A66FD7D03E835C0BA950C267F269BCE53C03B (void);
// 0x00000043 System.Void FriendScript::.ctor()
extern void FriendScript__ctor_m51AF92318F79A597166DE96035886551897E6D1B (void);
// 0x00000044 System.Void FriendScript/<ComeBack>d__14::.ctor(System.Int32)
extern void U3CComeBackU3Ed__14__ctor_mC74EA0807A6E158BC6A9666B670D2A97ED65F997 (void);
// 0x00000045 System.Void FriendScript/<ComeBack>d__14::System.IDisposable.Dispose()
extern void U3CComeBackU3Ed__14_System_IDisposable_Dispose_m31F49FEFDB2165C05240B59110111A1365613DDB (void);
// 0x00000046 System.Boolean FriendScript/<ComeBack>d__14::MoveNext()
extern void U3CComeBackU3Ed__14_MoveNext_m6977313E8B41275D27D4C3223FDF3C8525C3BA95 (void);
// 0x00000047 System.Object FriendScript/<ComeBack>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CComeBackU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m13C8C6D21FB3E83D188C00D3BD1F5B6FA07FE2AA (void);
// 0x00000048 System.Void FriendScript/<ComeBack>d__14::System.Collections.IEnumerator.Reset()
extern void U3CComeBackU3Ed__14_System_Collections_IEnumerator_Reset_m40B4F07E1E4485ED69988DEDED302A2CE03EBC49 (void);
// 0x00000049 System.Object FriendScript/<ComeBack>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CComeBackU3Ed__14_System_Collections_IEnumerator_get_Current_m23B5B0D5D97C8425C816649667DC939D72E8DC41 (void);
// 0x0000004A System.Void GameStart::Start()
extern void GameStart_Start_mD8FDF3BEC9120F83A26B58A73D1CDF675B70ADF7 (void);
// 0x0000004B System.Void GameStart::Update()
extern void GameStart_Update_mFDF2ADDC692311FA8315C6D35C8451681B42D793 (void);
// 0x0000004C System.Void GameStart::.ctor()
extern void GameStart__ctor_m402024398E63BF102E081A1C67BB5827C93A8B1A (void);
// 0x0000004D System.Void PlayerMovement::Start()
extern void PlayerMovement_Start_mB585552228B1908E44D3A69496598FB485F608B6 (void);
// 0x0000004E System.Void PlayerMovement::Update()
extern void PlayerMovement_Update_mC3491BD6CDFF1FA543B16969144C939B2298052F (void);
// 0x0000004F System.Void PlayerMovement::PlaySound()
extern void PlayerMovement_PlaySound_m81D1698B698C5C8A742AF2109BF51EE787CF2E40 (void);
// 0x00000050 System.Void PlayerMovement::WalkSound()
extern void PlayerMovement_WalkSound_m90B82A15AB5940C436745EB4E65C9948F5C54362 (void);
// 0x00000051 System.Void PlayerMovement::.ctor()
extern void PlayerMovement__ctor_mBF9F632DD9929DD6FF092A968649A4406BFE397F (void);
// 0x00000052 System.Void PlayerMovement::.cctor()
extern void PlayerMovement__cctor_m71F48D39E44B25D19A046C311349AB2445F957F0 (void);
// 0x00000053 System.Void ProfEvent::Start()
extern void ProfEvent_Start_m271D4840B2581853C6870CFDAC08833EF2B692A2 (void);
// 0x00000054 System.Void ProfEvent::Update()
extern void ProfEvent_Update_mF5EAC2956912FA48F91D70B4BF4E68C4FA639669 (void);
// 0x00000055 System.Collections.IEnumerator ProfEvent::Dilkmore()
extern void ProfEvent_Dilkmore_m3E3586F9AB818095499330D0E8CF9921E5B7E231 (void);
// 0x00000056 System.Collections.IEnumerator ProfEvent::Wegener()
extern void ProfEvent_Wegener_m809B72CC414FB7ADE6F724814BD78FDE6821D916 (void);
// 0x00000057 System.Collections.IEnumerator ProfEvent::Galifianakis()
extern void ProfEvent_Galifianakis_m1A941B09414BE7041F4CF1A90B0DC6FF7495B446 (void);
// 0x00000058 System.Collections.IEnumerator ProfEvent::OpeningSound()
extern void ProfEvent_OpeningSound_mA1CF54B9E12A5FC4C0ED7A6C728A6DDFA09EE339 (void);
// 0x00000059 System.Void ProfEvent::.ctor()
extern void ProfEvent__ctor_mC11EF87D9DDE4A39334BE416AA9454A571B5F5A0 (void);
// 0x0000005A System.Void ProfEvent/<Dilkmore>d__26::.ctor(System.Int32)
extern void U3CDilkmoreU3Ed__26__ctor_m74A78C78F1DCC68043ED881687093E11A1B175FB (void);
// 0x0000005B System.Void ProfEvent/<Dilkmore>d__26::System.IDisposable.Dispose()
extern void U3CDilkmoreU3Ed__26_System_IDisposable_Dispose_m7188F479FF8C8E7688B8B308F65F84F30CD0072D (void);
// 0x0000005C System.Boolean ProfEvent/<Dilkmore>d__26::MoveNext()
extern void U3CDilkmoreU3Ed__26_MoveNext_m3B34483523B8C5AEC8C6415316B07F8E060CFA2B (void);
// 0x0000005D System.Object ProfEvent/<Dilkmore>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDilkmoreU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4DAB88BCFCBC8944428DFFD9160DFD746C925248 (void);
// 0x0000005E System.Void ProfEvent/<Dilkmore>d__26::System.Collections.IEnumerator.Reset()
extern void U3CDilkmoreU3Ed__26_System_Collections_IEnumerator_Reset_mFC03480CC6EF6321372F4D0112FF095510A93E94 (void);
// 0x0000005F System.Object ProfEvent/<Dilkmore>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CDilkmoreU3Ed__26_System_Collections_IEnumerator_get_Current_m163696E72D0ADCBF7DB7D315AB46CFC2C10F2DD8 (void);
// 0x00000060 System.Void ProfEvent/<Wegener>d__27::.ctor(System.Int32)
extern void U3CWegenerU3Ed__27__ctor_m884191B8383FF0E2D118C0BEE467D4222A088153 (void);
// 0x00000061 System.Void ProfEvent/<Wegener>d__27::System.IDisposable.Dispose()
extern void U3CWegenerU3Ed__27_System_IDisposable_Dispose_m762CD4601F4060751E62034D8B14002B2C0A674F (void);
// 0x00000062 System.Boolean ProfEvent/<Wegener>d__27::MoveNext()
extern void U3CWegenerU3Ed__27_MoveNext_m9CD6A4423DC2527B3F56094DFFFDE9C24D65E366 (void);
// 0x00000063 System.Object ProfEvent/<Wegener>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWegenerU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB4FC103DFE38FBACCD31A8DCE976328EF91561AF (void);
// 0x00000064 System.Void ProfEvent/<Wegener>d__27::System.Collections.IEnumerator.Reset()
extern void U3CWegenerU3Ed__27_System_Collections_IEnumerator_Reset_m5CC5F42BE61899FDEB9A6CF2BA6350C2D0527AF4 (void);
// 0x00000065 System.Object ProfEvent/<Wegener>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CWegenerU3Ed__27_System_Collections_IEnumerator_get_Current_m1766E7456795CD3A55099FAA257A3F8653B6A85C (void);
// 0x00000066 System.Void ProfEvent/<Galifianakis>d__28::.ctor(System.Int32)
extern void U3CGalifianakisU3Ed__28__ctor_m0ED258D08EA42FA5E3A95691C4EC5CF993CAB6E8 (void);
// 0x00000067 System.Void ProfEvent/<Galifianakis>d__28::System.IDisposable.Dispose()
extern void U3CGalifianakisU3Ed__28_System_IDisposable_Dispose_mDCBC4062F51871556F133C8CCCFB3CD1DF83333E (void);
// 0x00000068 System.Boolean ProfEvent/<Galifianakis>d__28::MoveNext()
extern void U3CGalifianakisU3Ed__28_MoveNext_mEC10E33C2024D10FF1E7D9DD2ADF70C29C2C0390 (void);
// 0x00000069 System.Object ProfEvent/<Galifianakis>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGalifianakisU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8BB89622EE3E1D42F3EADDD6ED5EE76CA9499FA9 (void);
// 0x0000006A System.Void ProfEvent/<Galifianakis>d__28::System.Collections.IEnumerator.Reset()
extern void U3CGalifianakisU3Ed__28_System_Collections_IEnumerator_Reset_mC88D506B123BABF84B47AFB3879DE29F4B679CEB (void);
// 0x0000006B System.Object ProfEvent/<Galifianakis>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CGalifianakisU3Ed__28_System_Collections_IEnumerator_get_Current_m92BC5B93CBB500C8CCE133665A64DE3167CBE93C (void);
// 0x0000006C System.Void ProfEvent/<OpeningSound>d__29::.ctor(System.Int32)
extern void U3COpeningSoundU3Ed__29__ctor_m1AB01C927F90ED00CFAE8A5C82158EB001A427CF (void);
// 0x0000006D System.Void ProfEvent/<OpeningSound>d__29::System.IDisposable.Dispose()
extern void U3COpeningSoundU3Ed__29_System_IDisposable_Dispose_m8E717215B45659AAB3538D6CABD11AD3A3C9AE3C (void);
// 0x0000006E System.Boolean ProfEvent/<OpeningSound>d__29::MoveNext()
extern void U3COpeningSoundU3Ed__29_MoveNext_m5B13C5D88238C9D85D0C52F3753FF4C678A8D8C4 (void);
// 0x0000006F System.Object ProfEvent/<OpeningSound>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COpeningSoundU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14E182718730081CD4EF60622DCD53463C23E56E (void);
// 0x00000070 System.Void ProfEvent/<OpeningSound>d__29::System.Collections.IEnumerator.Reset()
extern void U3COpeningSoundU3Ed__29_System_Collections_IEnumerator_Reset_mBFC669D77050B079430CCDF52C0F29E4F42ADB44 (void);
// 0x00000071 System.Object ProfEvent/<OpeningSound>d__29::System.Collections.IEnumerator.get_Current()
extern void U3COpeningSoundU3Ed__29_System_Collections_IEnumerator_get_Current_mF6ED367165D4B150DA78DF88E0EE8C2ECAB15088 (void);
// 0x00000072 System.Void Refridgerator::Start()
extern void Refridgerator_Start_m9BFCC8D5F481A3D22B031DC591D3867E18515300 (void);
// 0x00000073 System.Void Refridgerator::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Refridgerator_OnTriggerEnter2D_m9C47AFDC6994B83FD2D9F3CC4B76F1CF4DD9275D (void);
// 0x00000074 System.Void Refridgerator::OnTriggerExit2D(UnityEngine.Collider2D)
extern void Refridgerator_OnTriggerExit2D_mBE14CAA462183E13AFA523FB35B20E956E49C4AD (void);
// 0x00000075 System.Void Refridgerator::Update()
extern void Refridgerator_Update_m5CC41BE5B6C34079519F006CBA2A9EBADE048CB0 (void);
// 0x00000076 System.Collections.IEnumerator Refridgerator::CloseFridge()
extern void Refridgerator_CloseFridge_mECB049F0FD280908EE24909A7D19FCD2E0D4E97C (void);
// 0x00000077 System.Void Refridgerator::.ctor()
extern void Refridgerator__ctor_m03CA51CE92EA478914FCB98D4B74551DF175998E (void);
// 0x00000078 System.Void Refridgerator/<CloseFridge>d__20::.ctor(System.Int32)
extern void U3CCloseFridgeU3Ed__20__ctor_m91CE310CFA79F8474FB983A760A1635DF9DA4676 (void);
// 0x00000079 System.Void Refridgerator/<CloseFridge>d__20::System.IDisposable.Dispose()
extern void U3CCloseFridgeU3Ed__20_System_IDisposable_Dispose_m8805BCEBB6B944DB2965E8D157A958A39F50A028 (void);
// 0x0000007A System.Boolean Refridgerator/<CloseFridge>d__20::MoveNext()
extern void U3CCloseFridgeU3Ed__20_MoveNext_m8C1CAE8F9E0BA1450E56EB8EF6560116FB89B7E2 (void);
// 0x0000007B System.Object Refridgerator/<CloseFridge>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCloseFridgeU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC737CD2B69C6BE32344F591BFF2703C1EA74961B (void);
// 0x0000007C System.Void Refridgerator/<CloseFridge>d__20::System.Collections.IEnumerator.Reset()
extern void U3CCloseFridgeU3Ed__20_System_Collections_IEnumerator_Reset_mA84DA17A6D3E0647A7BFAC0AA8C102D518F3B578 (void);
// 0x0000007D System.Object Refridgerator/<CloseFridge>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CCloseFridgeU3Ed__20_System_Collections_IEnumerator_get_Current_m768F475AF4DA707C96A4C1B1CB547CCFE3065B09 (void);
// 0x0000007E System.Void SleepingBed::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void SleepingBed_OnTriggerEnter2D_mE6ACA5804501A778D7C0D19DF89CC227537F6C24 (void);
// 0x0000007F System.Void SleepingBed::OnTriggerExit2D(UnityEngine.Collider2D)
extern void SleepingBed_OnTriggerExit2D_mC4E380C9DA6D5F31D1E1BF534A9A2D184739BC0A (void);
// 0x00000080 System.Void SleepingBed::Start()
extern void SleepingBed_Start_m1CAC9A3C8DAA7DD863F02FCCCC35D8A7835357E5 (void);
// 0x00000081 System.Void SleepingBed::Update()
extern void SleepingBed_Update_mF06ABAC1C60C4B6C1CAEA41ABF334E28D146DF02 (void);
// 0x00000082 System.Void SleepingBed::.ctor()
extern void SleepingBed__ctor_m892FF01C3FEAB816B1709B6A142EAF10AC573C4E (void);
static Il2CppMethodPointer s_methodPointers[130] = 
{
	Assignment_OnTriggerEnter2D_m69634A262155CB54A55FF6E73D6805A6E9537996,
	Assignment_OnTriggerExit2D_m9BA562CF00E41CBFBB3BFA469562EC423AC12B07,
	Assignment_Start_m036CD2F2E014EC7C1C39D548801D6BE681C8F625,
	Assignment_Update_mA52D445775AA9A260861FFF4058A1B2FAC88D80A,
	Assignment_GameOverSequence_m053B41B7BE1640A478893749BF18277856B18E56,
	Assignment_ScoreSequence_m108330CC445851E85677F49CA88015EFF056C414,
	Assignment_resetStatics_mA7F7163D23E442C22A5786599289030BEDA36E55,
	Assignment__ctor_m33010A19C5CC18368E18FA9232A9F586EAE37B4A,
	Assignment__cctor_m4917921C151EF2963E230CAE2EDD23038EB66DB4,
	U3CGameOverSequenceU3Ed__38__ctor_m2D5D76631A634FE147DB4F41DD4A0F5265ED58A8,
	U3CGameOverSequenceU3Ed__38_System_IDisposable_Dispose_mEE520F987D74AE398C332F13FD99879065A00013,
	U3CGameOverSequenceU3Ed__38_MoveNext_m6F8FC1C8E36AAD3D901E5F4DEED0519E54488894,
	U3CGameOverSequenceU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7D28F2C1D6EF1970DD18A275BAEE3E1EFC1C3129,
	U3CGameOverSequenceU3Ed__38_System_Collections_IEnumerator_Reset_m1FD62067D3185FE927F9BF7639452F1851329C41,
	U3CGameOverSequenceU3Ed__38_System_Collections_IEnumerator_get_Current_m185FBBE6329ACE7391C7E879B66FDCB986CD3F77,
	U3CScoreSequenceU3Ed__39__ctor_mAEF6264729299089B2280586619F541344088D82,
	U3CScoreSequenceU3Ed__39_System_IDisposable_Dispose_m3A87C2378875C252AD099024C4A3E0C12C6EF8C8,
	U3CScoreSequenceU3Ed__39_MoveNext_mEA3DE579B3993C478BD63AB655EF431D98B66557,
	U3CScoreSequenceU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0D41F2085C7F0D9EFF41E229CE747AF7C8B045CE,
	U3CScoreSequenceU3Ed__39_System_Collections_IEnumerator_Reset_mB02F97C1ED30CDB2D4292D0210380CA4933A3360,
	U3CScoreSequenceU3Ed__39_System_Collections_IEnumerator_get_Current_m5518076540D1507B7F206374F1CA5BE66A9ADA62,
	BedScript_Start_mCDA7CFD534EC65CB57B3AE1169EDEB0C0951F230,
	BedScript_Update_m90C210DB03D228FEE76BDC3A964C635CEDBE10F2,
	BedScript__ctor_mD7CF14E6E61808E8D196E39670C031FB678490D2,
	BuyingFood_Start_m4F9236312F81E09E97DC02B3630D842D0CCA91F2,
	BuyingFood_OnTriggerEnter2D_m98DDB7A0E376DC81681037FD30C00D778AF03174,
	BuyingFood_OnTriggerExit2D_m21345A2424528E6F57C9E87CA10BC4A3263AD2B4,
	BuyingFood_Update_m3843AAC1AD292A2CFFDBD7361D138AC5D234B363,
	BuyingFood_ComeBack_m194C119F380AE46C737A355B4452FA7FF25490F6,
	BuyingFood__ctor_mEADBB6BD9651F50F0CF9D6DAA8AE707A662D4053,
	U3CComeBackU3Ed__7__ctor_m28EE7E5BAF8359271F86C22BE628B4B9FF2CF186,
	U3CComeBackU3Ed__7_System_IDisposable_Dispose_m6EC106E82E8310369A4E6F2C120508D52A7373F6,
	U3CComeBackU3Ed__7_MoveNext_mEB04AD82D1BFF1FBA424A80EB5F560AAC0D02A99,
	U3CComeBackU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m56B30F705652C33EBE7460DA102F14C46CDDD025,
	U3CComeBackU3Ed__7_System_Collections_IEnumerator_Reset_mFC6E53B7484FB463E3CBEE2D7343C4FE8CCF97AA,
	U3CComeBackU3Ed__7_System_Collections_IEnumerator_get_Current_m267AE7046310A522766FD54D017626A2A2158113,
	ClockTime_Start_mA429B55638ABE8DAEE2ECC5B8F3D2F88F6BAC72B,
	ClockTime_Update_mA09A1FC0D030C62047EBF2183AA40AEB0B7CF906,
	ClockTime_changeMinute_mBCF9C130D7B010F6D5A888882DD746FEF7237A65,
	ClockTime_changeHour_mA2C2FE1A5A2DB7D2304B74EC54374E689A70461A,
	ClockTime__ctor_m931BA6807FE6950E4F243A6591AB9500A899C341,
	ClockTime__cctor_m872BD54DCCC42A4FD7E5AD77FD391DA47F3D26DB,
	CounterObject_Start_m95ECC341D7486148CF08083C654182262FC53A3D,
	CounterObject_Update_mC77DACE177DA105B352D4F1E8077DC4C957DB46A,
	CounterObject__ctor_mCA1731E6FB39CEFF8A8AB9EEB50A151B3EC0F05B,
	CounterObject__cctor_m80451F82A64AD2A36F4EB787108A15CB3EF5670B,
	Dialogue_Display_m7A893184DF6C1DFE4B1C3AC35FA32EA48E14F897,
	Dialogue_StopTyping_mD671FAEE764B6E569E89C472DE3EF870C45BB9BE,
	Dialogue_Type_m0971D31541433FE207337205D7BBD86A86D553A1,
	Dialogue__ctor_m76C11FD80AFD4F6BE87E30C737879EBB5A1D724E,
	U3CTypeU3Ed__9__ctor_m1B72D019C1EA5A0A2173E7E76DA67C90D52BF057,
	U3CTypeU3Ed__9_System_IDisposable_Dispose_m541ACFCBEAE51731393C3D48A9F5BBD33EFA9150,
	U3CTypeU3Ed__9_MoveNext_m4A208C7998709D7740F470D11CE84EB8553349FD,
	U3CTypeU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4A03A88D3AE993822722B3F4C124939AF7A1E2CB,
	U3CTypeU3Ed__9_System_Collections_IEnumerator_Reset_mD42721D7BC51BA10692735533B8F397787C0C415,
	U3CTypeU3Ed__9_System_Collections_IEnumerator_get_Current_m745F9253F292285753FE51DEF39C7C36F1062123,
	DoorTrigger_Start_m90DB9D0A9246E1EB09B43C23E4E329E8BE05BE7B,
	DoorTrigger_OnTriggerEnter2D_m3A67FAC61DBB4FBDA085A13C27DEEA4C88A932F5,
	DoorTrigger_OnTriggerExit2D_mAB29C2222ED5A40BA25FFC194F08183184773132,
	DoorTrigger_Update_m4E576A3AFB269F0B116ED353CA0FF1D19527F12E,
	DoorTrigger__ctor_m91AD0C323D39159697E05023696804017734CB6F,
	FriendScript_OnTriggerEnter2D_m047B38AD226422941E349B4233668147D1EA4D3E,
	FriendScript_OnTriggerExit2D_m941324B74AC497F7DB263D680AF46A7E97B8F1F0,
	FriendScript_Start_m026F9A96E13DDB3296FA27366655ACD45C3774FB,
	FriendScript_Update_m880EE63CDC29B786C8EA59BD96B507408D5A836B,
	FriendScript_ComeBack_m9D5A66FD7D03E835C0BA950C267F269BCE53C03B,
	FriendScript__ctor_m51AF92318F79A597166DE96035886551897E6D1B,
	U3CComeBackU3Ed__14__ctor_mC74EA0807A6E158BC6A9666B670D2A97ED65F997,
	U3CComeBackU3Ed__14_System_IDisposable_Dispose_m31F49FEFDB2165C05240B59110111A1365613DDB,
	U3CComeBackU3Ed__14_MoveNext_m6977313E8B41275D27D4C3223FDF3C8525C3BA95,
	U3CComeBackU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m13C8C6D21FB3E83D188C00D3BD1F5B6FA07FE2AA,
	U3CComeBackU3Ed__14_System_Collections_IEnumerator_Reset_m40B4F07E1E4485ED69988DEDED302A2CE03EBC49,
	U3CComeBackU3Ed__14_System_Collections_IEnumerator_get_Current_m23B5B0D5D97C8425C816649667DC939D72E8DC41,
	GameStart_Start_mD8FDF3BEC9120F83A26B58A73D1CDF675B70ADF7,
	GameStart_Update_mFDF2ADDC692311FA8315C6D35C8451681B42D793,
	GameStart__ctor_m402024398E63BF102E081A1C67BB5827C93A8B1A,
	PlayerMovement_Start_mB585552228B1908E44D3A69496598FB485F608B6,
	PlayerMovement_Update_mC3491BD6CDFF1FA543B16969144C939B2298052F,
	PlayerMovement_PlaySound_m81D1698B698C5C8A742AF2109BF51EE787CF2E40,
	PlayerMovement_WalkSound_m90B82A15AB5940C436745EB4E65C9948F5C54362,
	PlayerMovement__ctor_mBF9F632DD9929DD6FF092A968649A4406BFE397F,
	PlayerMovement__cctor_m71F48D39E44B25D19A046C311349AB2445F957F0,
	ProfEvent_Start_m271D4840B2581853C6870CFDAC08833EF2B692A2,
	ProfEvent_Update_mF5EAC2956912FA48F91D70B4BF4E68C4FA639669,
	ProfEvent_Dilkmore_m3E3586F9AB818095499330D0E8CF9921E5B7E231,
	ProfEvent_Wegener_m809B72CC414FB7ADE6F724814BD78FDE6821D916,
	ProfEvent_Galifianakis_m1A941B09414BE7041F4CF1A90B0DC6FF7495B446,
	ProfEvent_OpeningSound_mA1CF54B9E12A5FC4C0ED7A6C728A6DDFA09EE339,
	ProfEvent__ctor_mC11EF87D9DDE4A39334BE416AA9454A571B5F5A0,
	U3CDilkmoreU3Ed__26__ctor_m74A78C78F1DCC68043ED881687093E11A1B175FB,
	U3CDilkmoreU3Ed__26_System_IDisposable_Dispose_m7188F479FF8C8E7688B8B308F65F84F30CD0072D,
	U3CDilkmoreU3Ed__26_MoveNext_m3B34483523B8C5AEC8C6415316B07F8E060CFA2B,
	U3CDilkmoreU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4DAB88BCFCBC8944428DFFD9160DFD746C925248,
	U3CDilkmoreU3Ed__26_System_Collections_IEnumerator_Reset_mFC03480CC6EF6321372F4D0112FF095510A93E94,
	U3CDilkmoreU3Ed__26_System_Collections_IEnumerator_get_Current_m163696E72D0ADCBF7DB7D315AB46CFC2C10F2DD8,
	U3CWegenerU3Ed__27__ctor_m884191B8383FF0E2D118C0BEE467D4222A088153,
	U3CWegenerU3Ed__27_System_IDisposable_Dispose_m762CD4601F4060751E62034D8B14002B2C0A674F,
	U3CWegenerU3Ed__27_MoveNext_m9CD6A4423DC2527B3F56094DFFFDE9C24D65E366,
	U3CWegenerU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB4FC103DFE38FBACCD31A8DCE976328EF91561AF,
	U3CWegenerU3Ed__27_System_Collections_IEnumerator_Reset_m5CC5F42BE61899FDEB9A6CF2BA6350C2D0527AF4,
	U3CWegenerU3Ed__27_System_Collections_IEnumerator_get_Current_m1766E7456795CD3A55099FAA257A3F8653B6A85C,
	U3CGalifianakisU3Ed__28__ctor_m0ED258D08EA42FA5E3A95691C4EC5CF993CAB6E8,
	U3CGalifianakisU3Ed__28_System_IDisposable_Dispose_mDCBC4062F51871556F133C8CCCFB3CD1DF83333E,
	U3CGalifianakisU3Ed__28_MoveNext_mEC10E33C2024D10FF1E7D9DD2ADF70C29C2C0390,
	U3CGalifianakisU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8BB89622EE3E1D42F3EADDD6ED5EE76CA9499FA9,
	U3CGalifianakisU3Ed__28_System_Collections_IEnumerator_Reset_mC88D506B123BABF84B47AFB3879DE29F4B679CEB,
	U3CGalifianakisU3Ed__28_System_Collections_IEnumerator_get_Current_m92BC5B93CBB500C8CCE133665A64DE3167CBE93C,
	U3COpeningSoundU3Ed__29__ctor_m1AB01C927F90ED00CFAE8A5C82158EB001A427CF,
	U3COpeningSoundU3Ed__29_System_IDisposable_Dispose_m8E717215B45659AAB3538D6CABD11AD3A3C9AE3C,
	U3COpeningSoundU3Ed__29_MoveNext_m5B13C5D88238C9D85D0C52F3753FF4C678A8D8C4,
	U3COpeningSoundU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14E182718730081CD4EF60622DCD53463C23E56E,
	U3COpeningSoundU3Ed__29_System_Collections_IEnumerator_Reset_mBFC669D77050B079430CCDF52C0F29E4F42ADB44,
	U3COpeningSoundU3Ed__29_System_Collections_IEnumerator_get_Current_mF6ED367165D4B150DA78DF88E0EE8C2ECAB15088,
	Refridgerator_Start_m9BFCC8D5F481A3D22B031DC591D3867E18515300,
	Refridgerator_OnTriggerEnter2D_m9C47AFDC6994B83FD2D9F3CC4B76F1CF4DD9275D,
	Refridgerator_OnTriggerExit2D_mBE14CAA462183E13AFA523FB35B20E956E49C4AD,
	Refridgerator_Update_m5CC41BE5B6C34079519F006CBA2A9EBADE048CB0,
	Refridgerator_CloseFridge_mECB049F0FD280908EE24909A7D19FCD2E0D4E97C,
	Refridgerator__ctor_m03CA51CE92EA478914FCB98D4B74551DF175998E,
	U3CCloseFridgeU3Ed__20__ctor_m91CE310CFA79F8474FB983A760A1635DF9DA4676,
	U3CCloseFridgeU3Ed__20_System_IDisposable_Dispose_m8805BCEBB6B944DB2965E8D157A958A39F50A028,
	U3CCloseFridgeU3Ed__20_MoveNext_m8C1CAE8F9E0BA1450E56EB8EF6560116FB89B7E2,
	U3CCloseFridgeU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC737CD2B69C6BE32344F591BFF2703C1EA74961B,
	U3CCloseFridgeU3Ed__20_System_Collections_IEnumerator_Reset_mA84DA17A6D3E0647A7BFAC0AA8C102D518F3B578,
	U3CCloseFridgeU3Ed__20_System_Collections_IEnumerator_get_Current_m768F475AF4DA707C96A4C1B1CB547CCFE3065B09,
	SleepingBed_OnTriggerEnter2D_mE6ACA5804501A778D7C0D19DF89CC227537F6C24,
	SleepingBed_OnTriggerExit2D_mC4E380C9DA6D5F31D1E1BF534A9A2D184739BC0A,
	SleepingBed_Start_m1CAC9A3C8DAA7DD863F02FCCCC35D8A7835357E5,
	SleepingBed_Update_mF06ABAC1C60C4B6C1CAEA41ABF334E28D146DF02,
	SleepingBed__ctor_m892FF01C3FEAB816B1709B6A142EAF10AC573C4E,
};
static const int32_t s_InvokerIndices[130] = 
{
	1228,
	1228,
	1467,
	1467,
	1425,
	1425,
	1467,
	1467,
	2302,
	1218,
	1467,
	1445,
	1425,
	1467,
	1425,
	1218,
	1467,
	1445,
	1425,
	1467,
	1425,
	1467,
	1467,
	1467,
	1467,
	1228,
	1228,
	1467,
	1425,
	1467,
	1218,
	1467,
	1445,
	1425,
	1467,
	1425,
	1467,
	1467,
	1467,
	1467,
	1467,
	2302,
	1467,
	1467,
	1467,
	2302,
	486,
	1467,
	1425,
	1467,
	1218,
	1467,
	1445,
	1425,
	1467,
	1425,
	1467,
	1228,
	1228,
	1467,
	1467,
	1228,
	1228,
	1467,
	1467,
	1425,
	1467,
	1218,
	1467,
	1445,
	1425,
	1467,
	1425,
	1467,
	1467,
	1467,
	1467,
	1467,
	1467,
	1467,
	1467,
	2302,
	1467,
	1467,
	1425,
	1425,
	1425,
	1425,
	1467,
	1218,
	1467,
	1445,
	1425,
	1467,
	1425,
	1218,
	1467,
	1445,
	1425,
	1467,
	1425,
	1218,
	1467,
	1445,
	1425,
	1467,
	1425,
	1218,
	1467,
	1445,
	1425,
	1467,
	1425,
	1467,
	1228,
	1228,
	1467,
	1425,
	1467,
	1218,
	1467,
	1445,
	1425,
	1467,
	1425,
	1228,
	1228,
	1467,
	1467,
	1467,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	130,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
