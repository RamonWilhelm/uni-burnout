# Uni Burnout

## Table of Contents  
- [Introduction](#introduction) 
- [Control](#control)
- [Features](#features)
- [Updates](#updates)
- [Future](#future)
- [Notes](#notes)

## Introduction

![Screenshot001](https://bitbucket.org/RamonWilhelm/uni-burnout/raw/4423eca4194ab1190271f96d8c2662c8f4b9e3c5/Screenshots/Screenshot001.jpg) ![Screenshot002](https://bitbucket.org/RamonWilhelm/uni-burnout/raw/4423eca4194ab1190271f96d8c2662c8f4b9e3c5/Screenshots/Screenshot002.jpg)


Uni Burnout was a final project for the Master's course [Mobile Games and Entertainment](https://lnu.se/en/course/mobile-games-and-entertainment/vaxjo-international-part-time-spring/) of the study program computer science at the [Linnaeus University](https://lnu.se/) in Växjö, Sweden. 
It is a serious game about a student who receives assignments from his lecturers of three courses that must be completed within a very short deadline.
The location of this game is the home of the student. He must try to complete the assignments from his three courses 
as far as possible under enormous time pressure without endangering his health.
The time passes very fast in this game. As the student works on his assignments, the stress level increases. Once the stress level reaches its maximum, 
the student gets burnout.  Due to the increasing stress level, the student also has to do activities such as sleeping, eating, and hanging out with friends to reduce the stress level.

![Screenshot003](https://bitbucket.org/RamonWilhelm/uni-burnout/raw/4423eca4194ab1190271f96d8c2662c8f4b9e3c5/Screenshots/Screenshot003.jpg)

The goal of the game is to complete as many assignments as possible without suffering burnout.

This game was developed with **Unity 2020.2.1f1 (64-Bit)**.
It can be played in the web browser [**here**](https://ramoramainteractive.itch.io/uni-burnout).

## Control
* Use the arrow keys to move left or right.
* Use the up key to interact with certain objects. For sleeping on the bed and working on the assignments keep the up key pressed as long as necessary.

## Features
* Stress level bar which must not reach its maximum.
* Day counter.
* The counter (PAL), how many times you can not meet your friends until they stop coming at 2 pm..
* Number of Foods in the refridgerator
* Hunger bar, which accelerates the decline of the energy bar when it is empty.
* Energy bar, which accelerates the increase of the stress when it is empty.
* Acceleration of stress level from 8 p.m. to 8 a.m.
* Includes in-game game instructions.
* Score at the end of the game if the student did not get a burnout.

## Updates
No Updates yet.

## Future
Besides bug fixing, the browser game will also be adapted to mobile devices and made playable.

## Notes
* Make sure, you open the game with [**Unity 2020.2.1f1 (64-bit)**](https://unity3d.com/de/get-unity/download/archive).
* Graphics are created with the pixel art tool [**aseperite**](https://www.aseprite.org/).
* This project has been stored via [**Git LFS**](https://git-lfs.github.com/).
